#ifndef __T_VECTOR_HPP__
#define __T_VECTOR_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class Vector;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::Vector<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::Vector<T>& rhv);

namespace cd01 {

template <typename T>
class Vector
{
    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const Vector<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, Vector<Type>& rhv);
    static const double RESERVE_COEFF = 2;
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend Vector<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        const_iterator& operator+=(const int value);
        const_iterator& operator-=(const int value);
        const_iterator operator+(const int value) const;
        const_iterator operator-(const int value) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    protected:
        explicit const_iterator(T* ptr);
        void setPtr(T* ptr);
        pointer getPtr() const;
        void setPtr(const T* ptr);
    private:
        pointer ptr_;
    };
 
    class iterator : public const_iterator {
        friend Vector<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        iterator(const_iterator& rhv);
        ~iterator();
        value_type* operator->() const;
        iterator& operator=(const iterator& rhv);
        value_type& operator*() const;
        iterator& operator--();
        iterator& operator+=(const int value);
        iterator& operator-=(const int value);
        iterator operator+(const int value) const;
        iterator operator-(const int value) const;
        iterator& operator++();
        iterator operator++(int);
        iterator operator--(int);
    private:
        explicit iterator(T* ptr);
    };

    class const_reverse_iterator : public const_iterator {
        friend Vector<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const_reverse_iterator& operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator operator--();
        const_reverse_iterator& operator--(int);
        const_reverse_iterator& operator+=(const int value);
        const_reverse_iterator& operator-=(const int value);
        const_reverse_iterator operator+(const int value) const;
        const_reverse_iterator operator-(const int value) const;
        const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
    private:
        explicit const_reverse_iterator(T* ptr);
    };

    class reverse_iterator : public const_reverse_iterator {
        friend Vector<T>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();
        reverse_iterator& operator++();
        reverse_iterator operator++(int);
        reverse_iterator operator--();
        reverse_iterator& operator--(int);
        reverse_iterator& operator+=(const int value);
        reverse_iterator& operator-=(const int value);
        reverse_iterator operator+(const int value) const;
        reverse_iterator operator-(const int value) const;
        reverse_iterator& operator=(const reverse_iterator& rhv);
    private:
        explicit reverse_iterator(T* ptr);
    };

public:
    Vector();
    Vector(const size_type size);
    Vector(const int size, const T& initialValue);
    Vector(const Vector<T>& rhv);
    template <typename InputIterator>
    Vector(InputIterator first, InputIterator last);
    ~Vector();
    size_type size() const;
    size_type capacity() const;
    void reserve(const size_type n);
    void resize(const size_type n);
    void resize(const size_type n, const T& initialValue);
    void push_back(const T& elem);
    void pop_back();
    T& operator[](const size_type index);
    T operator[](const size_type index) const;
    const Vector& operator=(const Vector<T>& rhv);
    bool operator<(const Vector<T>& rhv) const;
    bool operator==(const Vector<T>& rhv) const;
    bool operator!=(const Vector<T>& rhv) const;
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    iterator insert(iterator pos, const T& arg);
    iterator insert(iterator pos, const int n, const T& arg);
    template <typename InputIterator>
    void insert(iterator pos, InputIterator l, InputIterator r);
    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);
private:
    T* begin_;
    T* end_;
    T* bufferEnd_;
}; /// end of template class Vector

} /// end of namespace cd01

#include "sources/Vector.cpp"

#endif /// __T_VECTOR_HPP__

