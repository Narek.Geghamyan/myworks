#ifndef __STACK_HPP__
#define __STACK_HPP__

#include "headers/SingleList.hpp"

namespace cd01 {

template <typename T, typename Sequance = SingleList<T> >
class Stack : private Sequance
{
public:
    void push(const T& d);
    void pop();
    T& top();
    const T& top() const;
    Stack& operator=(const Stack& rhv);
    bool empty() const;
    size_t size() const;
    bool operator==(const Stack& rhv) const;
    bool operator<(const Stack& rhv) const;
};

} /// end of namespace cd01

#include "sources/Stack.cpp"

#endif /// __STACK_HPP__

