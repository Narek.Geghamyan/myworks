#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include "headers/SingleList.hpp"

namespace cd01 {

template <typename T, typename Sequance = SingleList<T> >
class Queue : private Sequance
{
public:
    void push(const T& d);
    void pop();
    T& top();
    const T& top() const;
    Queue& operator=(const Queue& rhv);
    bool empty() const;
    size_t size() const;
    bool operator==(const Queue& rhv) const;
    bool operator<(const Queue& rhv) const;
};

} /// end of namespace cd01

#include "sources/Queue.cpp"

#endif /// __QUEUE_HPP__

