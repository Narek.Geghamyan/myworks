#include "headers/Vector.hpp"

#include <cassert>
#include <cstdlib>
#include <iostream>

/// Vector<T>

template <typename T>
std::ostream&
operator<<(std::ostream& out, const cd01::Vector<T>& rhv)
{
    out << "( ";
    for (T* ptr = rhv.begin_; ptr != rhv.end_; ++ptr) {
        out << *ptr << ' ';
    }
    out << ')';
    return out;
}

template <typename T>
std::istream&
operator>>(std::istream& in, cd01::Vector<T>& rhv)
{
    const char obr = static_cast<char>(in.get());
    if (obr != '(' && obr != '{' && obr != '[') {
        in.setstate(std::ios_base::failbit);
        return in;
    }

    for (T* ptr = rhv.begin_; ptr != rhv.end_; ++ptr) {
        in >> *ptr;
    }

    const char cbr = static_cast<char>(in.get());
    if (cbr != ')' && cbr != '}' && cbr != ']') {
        in.setstate(std::ios_base::failbit);
        return in;
    }
    return in;
}

namespace cd01 {

template <typename T>
Vector<T>::Vector()
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(end_)
{
}

template <typename T>
Vector<T>::Vector(const size_type size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(end_)
{
    resize(size);
}

template <typename T>
Vector<T>::Vector(const int size, const T& initialValue) 
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(end_)

{
    resize(size, initialValue);
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(end_)
{
    resize(rhv.size());
    for (T* ptr1 = begin_, *ptr2 = rhv.begin_; ptr1 != end_; ++ptr1, ++ptr2) {
        *ptr1 = *ptr2;
    }
}

template <typename T>
template <typename InputIterator>
Vector<T>::Vector(InputIterator first, InputIterator last)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(end_)
{
    insert(begin(), first, last);
}

template <typename T>
Vector<T>::~Vector()
{
    resize(0);
    if (begin_ != NULL) {
        ::free(begin_);
        begin_ = end_ = NULL;
    }
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    assert(begin_ != NULL);
    return const_iterator(begin_);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const
{
    assert(end_ != NULL);
    return const_iterator(end_);
}


template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (&rhv == this) {
        return *this;
    }

    if (size() != rhv.size()) {
        resize(rhv.size());
    }
    for (size_t i = 0; i < size(); ++i) {
        (*this)[i] = rhv[i];
    }
    return *this;
}

template <typename T>
T&
Vector<T>::operator[](const size_type index)
{
    assert(begin_ + index < end_ && "Invalid index");
    return *(begin_ + index);
}

template <typename T>
T
Vector<T>::operator[](const size_type index) const
{
    assert(begin_ + index < end_ && "Invalid index");
    return *(begin_ + index);
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename T>
bool
Vector<T>::operator==(const Vector<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    for (size_type i = 0; i < size(); ++i) {
        if ((*this)[i] != rhv[i]) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
Vector<T>::operator!=(const Vector<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
Vector<T>::operator<(const Vector<T>& rhv) const
{
    if (size() < rhv.size()) { 
        return true;
    }
    if (size() > rhv.size()) {
        return false;
    }
    for (size_type i = 0; i < size(); ++i) {
        if ((*this)[i] < rhv[i]) {
            return true;
        }
    }
    return false;
}

template <typename T>
void
Vector<T>::reserve(const size_type n)
{
    if (n <= capacity()) {
        return;
    }

    T* temp = reinterpret_cast<T*>(::malloc(n * sizeof(T)));
    for (T* oldPtr = begin_, *newPtr = temp; oldPtr != end_; ++oldPtr, ++newPtr) {
        *newPtr = T(*oldPtr);
        oldPtr->~T();
    }
    end_ = temp + size();
    ::free(begin_);
    begin_ = temp;
    bufferEnd_ = begin_ + n;
}

template <typename T>
void
Vector<T>::resize(const size_type n)
{
    if (n > capacity()) {
        reserve(n);
    }

    if (n == size()) {
        return;
    }

    if (n < size()) {
        while (end_ != begin_ + n) {
            (--end_)->~T();
        }
        return;
    }

    while (end_ != begin_ + n) {
        *end_++ = T();
    }
}

template <typename T>
void
Vector<T>::resize(const size_type n, const T& initialValue)
{
    if (n > capacity()) {
        reserve(n);
    }

    if (n == size()) {
        return;
    }

    if (n < size()) {
        while (end_ != begin_ + n) {
            (--end_)->~T();
        }
        return;
    }

    while (end_ != begin_ + n) {
        *end_++ = initialValue;
    }
}

template <typename T>
void
Vector<T>::push_back(const T& elem)
{
    if (size() == capacity()) {
        const size_type newSize = static_cast<size_type>(RESERVE_COEFF) * capacity();
        reserve(newSize == capacity() ? newSize + 1 : newSize);
    }
    *(end_) = elem;
    ++end_;
}

template <typename T>
void
Vector<T>::pop_back()
{
    if (end_ == begin_) {
        return;
    }
    end_--->~T();
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(Vector<T>::iterator pos, const T& arg)
{
    if (size() == capacity()) {
        difference_type diff = pos.getPtr() - begin_;
        const size_type newSize = static_cast<size_type>(RESERVE_COEFF) * capacity();
        reserve(newSize == capacity() ? newSize + 1 : newSize);
        pos = begin() + diff;
    }

    for (Vector<T>::iterator it1 = end(), it2 = it1--; it2 != pos; --it1, --it2) {
        *it2 = *it1;
    }
    ++end_;
    *pos = arg;
    return pos;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(Vector<T>::iterator pos, const int n, const T& arg)
{
    const size_type newSize = size() + n;
    if (newSize > capacity()) { 
        difference_type diff = pos.getPtr() - begin_;
        size_type newCapacity = capacity();
        if (0 == newCapacity) { newCapacity = 1; }
        while (newCapacity < newSize) {
            newCapacity *= static_cast<size_type>(RESERVE_COEFF);
        }
        reserve(newCapacity);
        pos = begin() + diff;
    }

    for (Vector<T>::iterator it1 = end() - 1, it2 = it1 + n; it1 != pos - 1; --it1, --it2) {
        *it2 = *it1;
    }
    end_ += n;

    for (int i = 0; i < n; ++i) {
        *(pos + i) = arg;
    }
    return pos;
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::insert(Vector<T>::iterator pos, InputIterator first, InputIterator last)
{
    size_t n = 0;
    for (InputIterator it = first; it != last; ++it, ++n);

    const size_type newSize = size() + n;
    if (newSize > capacity()) {
        difference_type diff = pos.getPtr() - begin_;
        size_type newCapacity = capacity();
        if (0 == newCapacity) { newCapacity = 1; }
        while (newCapacity < newSize) {
            newCapacity *= static_cast<size_type>(RESERVE_COEFF);
        }
        reserve(newCapacity);
        pos = begin() + diff;
    }

    for (Vector<T>::iterator it1 = end() - 1, it2 = it1 + n; it1 != pos - 1; --it1, --it2) {
        *it2 = *it1;
    }
    end_ += n;

    for (; first != last; ++first, ++pos) {
        *pos = *first;
    }
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator pos)
{
    for (Vector<T>::iterator it1 = pos, it2 = it1++; it2 != end(); ++it1, ++it2) {
        *it2 = *it1;
    }
    --end_;
    return pos;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator first, iterator last)
{

    int counter = 0;
    for (iterator it = first; it != last; ++it, counter++);

    for (iterator it2 = first++; first != last; ++first) {
        *it2 = *first;
    }
    end_ -= counter;
    return first;
}

/// Vector<T>::const_iterator

template <typename T>
Vector<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Vector<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
Vector<T>::const_iterator::const_iterator(T* ptr)
    : ptr_(ptr)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(const Vector<T>::const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
const typename Vector<T>::value_type&
Vector<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return *ptr_;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator=(const const_iterator& rhv)
{
    if (ptr_ == rhv.ptr_) {
        return *this;
    }
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::value_type* 
Vector<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator++()
{
    assert(ptr_ != NULL);
    ++ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_);
    ++ptr_;
    return tmp;
}

template <typename T>
bool
Vector<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    assert(ptr_ != NULL);
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    assert(ptr_ != NULL);
    return !(ptr_ == rhv.ptr_);
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator--()
{
    assert(ptr_ != NULL);
    --ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--(int)
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_);
    --ptr_;
    return tmp;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator+=(const int value)
{
    assert(ptr_ != NULL);
    ptr_ += value;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator-=(const int value)
{    
    assert(ptr_ != NULL);
    ptr_ -= value;
    return *this;
}

template <typename T>
bool
Vector<T>::const_iterator::operator<(const const_iterator& rhv) const
{
    assert(ptr_ != NULL || rhv.ptr_ != NULL);
    return ptr_ < rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator<=(const const_iterator& rhv) const
{
    assert(ptr_ != NULL || rhv.ptr_ != NULL);
    return ptr_ <= rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator>(const const_iterator& rhv) const
{
    assert(ptr_ != NULL || rhv.ptr_ != NULL);
    return ptr_ > rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator>=(const const_iterator& rhv) const
{
    assert(ptr_ != NULL || rhv.ptr_ != NULL);
    return ptr_ >= rhv.ptr_;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator+(const int value) const
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_ + value);
    return tmp;
    
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator-(const int value) const
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_ - value);
    return tmp;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
void
Vector<T>::const_iterator::setPtr(const T* ptr)
{
    ptr_ = ptr;
}

template <typename T>
void
Vector<T>::const_iterator::setPtr(T* ptr) { ptr_ = ptr; }

/// Vector<T>::iterator

template <typename T>
Vector<T>::iterator::iterator()
{
}

template <typename T>
Vector<T>::iterator::iterator(T* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
Vector<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
Vector<T>::iterator::~iterator()
{
}

template <typename T>
Vector<T>::iterator::iterator(const_iterator& rhv)
{
    const_iterator::setPtr(rhv.getPtr());
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    assert(end_ != NULL);
    return iterator(end_);
}

template <typename T>
typename Vector<T>::value_type*
Vector<T>::iterator::operator->() const
{
    assert(const_iterator::getPtr() != NULL);
    return const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator=(const iterator& rhv)
{ 
    if (const_iterator::getPtr() == rhv.ptr_) {
        return *this;
    }
    const_iterator::setPtr(rhv.ptr_); 
    return *this;
}

template <typename T>
typename Vector<T>::value_type&
Vector<T>::iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return *(const_iterator::getPtr());
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator--()
{
    assert(const_iterator::getPtr() != NULL);
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator--(int)
{
    assert(const_iterator::getPtr() != NULL);
    const iterator tmp(const_iterator::getPtr());
    const_iterator::setPtr(const_iterator::getPtr() - 1); 
    return tmp;
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator+=(const int value)
{
    assert(const_iterator::getPtr() != NULL);
    const_iterator::setPtr(const_iterator::getPtr() + value);
    return *this;
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator-=(const int value)
{    
    assert(const_iterator::getPtr() != NULL);
    const_iterator::setPtr(const_iterator::getPtr() - value);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator+(const int value) const
{
    assert(const_iterator::getPtr() != NULL);
    const iterator tmp(const_iterator::getPtr() + value);
    return tmp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator-(const int value) const
{
    assert(const_iterator::getPtr() != NULL);
    const iterator tmp(const_iterator::getPtr() - value);
    return tmp;
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator++()
{
    assert(const_iterator::getPtr() != NULL);
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator++(int)
{
    assert(const_iterator::getPtr() != NULL);
    const iterator tmp(const_iterator::getPtr());
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return tmp;
}

/// Vector<T>::const_reverse_iterator

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator()
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(T* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::~const_reverse_iterator()
{
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    assert(end_ != NULL);
    return const_reverse_iterator(end_).operator++();
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    assert(begin_ != NULL); 
    return const_reverse_iterator(begin_).operator++();
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++(int)
{
    const const_reverse_iterator tmp(const_iterator::getPtr());
    const_iterator::operator--();
    return tmp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--()
{
    const const_reverse_iterator tmp(const_iterator::getPtr());
    const_iterator::operator++();
    return tmp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator--(int)
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const int value) const
{ 
    const const_reverse_iterator tmp(const_iterator::getPtr() - value);
    return tmp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const int value) const
{
    const const_reverse_iterator tmp(const_iterator::getPtr() + value);    
    return tmp;
}

template <typename T> 
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    if (const_iterator::getPtr() == rhv.getPtr()) {
        return *this;
    }
    const_iterator::setPtr(rhv.getPtr());
    return *this;
}

/// Vector<T>::reverse_iterator

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator()
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(T* ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
Vector<T>::reverse_iterator::~reverse_iterator()
{
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    assert(end_ != NULL); 
    return reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    assert(begin_ != NULL);
    return reverse_iterator(begin_);
}

template <typename T> 
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{
    if (const_iterator::getPtr() == rhv.getPtr()) {
        return *this;
    }
    const_iterator::setPtr(rhv.getPtr());
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator++(int)
{
    const reverse_iterator tmp(const_iterator::getPtr());
    const_iterator::operator--();
    return tmp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator--()
{
    const reverse_iterator tmp(const_iterator::getPtr());
    const_iterator::operator++();
    return tmp;
}

template <typename T>
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator--(int)
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator+(const int value) const
{ 
    const reverse_iterator tmp(const_iterator::getPtr() - value);
    return tmp;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator-(const int value) const
{
    const reverse_iterator tmp(const_iterator::getPtr() + value);    
    return tmp;
}


} /// end of namespace cd01

