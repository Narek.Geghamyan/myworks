#include "headers/Stack.hpp"

namespace cd01 {

template <typename T, typename Sequence>
void
Stack<T, Sequence>::push(const T& d)
{
    Sequence::push_front(d);
}

template <typename T, typename Sequence>
void
Stack<T, Sequence>::pop()
{
    Sequence::pop_front();
}

template <typename T, typename Sequence>
T&
Stack<T, Sequence>::top()
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
const T&
Stack<T, Sequence>::top() const
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
Stack<T, Sequence>&
Stack<T, Sequence>::operator=(const Stack& rhv)
{
    Sequence::operator=(rhv.Sequence);
    return *this;
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
size_t
Stack<T, Sequence>::size() const
{
    return Sequence::size();
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::operator==(const Stack& rhv) const
{
    return Sequence::operator==(rhv);
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::operator<(const Stack& rhv) const
{
    return Sequence::operator<(rhv);
}

} /// end of namespace cd01

