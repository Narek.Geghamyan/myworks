#include "headers/List.hpp"

#include <cassert>
#include <cstdlib>
#include <iostream>

/// List<T>

template <typename T>
std::ostream&
operator<<(std::ostream& out, const cd01::List<T>& rhv)
{
    typedef typename cd01::List<T>::const_iterator const_iterator;
    out << "( ";
    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        out << *it << ' ';
    }
    out << ')';
    return out;
}

template <typename T>
std::istream&
operator>>(std::istream& in, cd01::List<T>& rhv)
{
    typedef typename cd01::List<T>::const_iterator const_iterator;
    const char obr = static_cast<char>(in.get());
    if (obr != '(' && obr != '{' && obr != '[') {
        in.setstate(std::ios_base::failbit);
        return in;
    }

    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        in >> *it;
    }

    const char cbr = static_cast<char>(in.get());
    if (cbr != ')' && cbr != '}' && cbr != ']') {
        in.setstate(std::ios_base::failbit);
        return in;
    }
    return in;
}

namespace cd01 {

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
List<T>::List(const size_type size)
    : begin_(NULL)
    , end_(NULL)
{
    resize(size);
}

template <typename T>
List<T>::List(const int size, const T& initialValue) 
    : begin_(NULL)
    , end_(NULL)
{
    resize(size, initialValue);
}

template <typename T>
List<T>::List(const List<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    insert_after(begin(), rhv.begin(), rhv.begin());
}

template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator first, InputIterator last)
    : begin_(NULL)
    , end_(NULL)
{
    insert_after(begin(), first, last);
}

template <typename T>
List<T>::~List()
{
    resize(0);
    if (begin_ != NULL) {
        begin_ = end_ = NULL;
    }
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator();
}


template <typename T>
const List<T>&
List<T>::operator=(const List<T>& rhv)
{
    typedef typename cd01::List<T>::const_iterator const_iterator;
    typedef typename cd01::List<T>::iterator iterator;
    if (&rhv == this) {
        return *this;
    }


    if (size() != rhv.size()) { 
        resize(rhv.size());
        iterator it1 = begin();
        for (const_iterator it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
            *it1 = *it2;
        }
    }
    return *this;
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    size_type counter = 0;
    for (const_iterator it = begin(); it != end(); ++it, ++counter);
    return counter;
}

template <typename T>
bool
List<T>::empty() const
{
   return size() == 0;
}

template <typename T>
bool
List<T>::operator==(const List<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    for (const_iterator it1 = begin(), it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
        if (*it1 != *it2) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
List<T>::operator!=(const List<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
List<T>::operator<(const List<T>& rhv) const
{
    typedef cd01::List<T>::const_iterator const_iterator;

    if (size() < rhv.size()) {
        return true;
    }

    for (const_iterator it1 = begin(), it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
        if (*it1 > *it2) {
            return false;
        }
    }
    return true;
}

template <typename T>
void
List<T>::resize(const size_type n)
{
    size_type listSize = size(); 
    if (n == listSize) {
        return;
    }
    
    if (n > listSize) {
        for (size_t i = 0; i < n - listSize; ++i) {
            push_back(T());
        }
        return;
    }

    iterator pos = begin();
    for (size_t i = 0; i < n; ++i, ++pos);
    erase(pos, end());
}

template <typename T>
void
List<T>::resize(const size_type n, const T& initialValue)
{
    size_type listSize = size(); 
    if (n == listSize) {
        return;
    }
    
    if (n > listSize) {
        for (size_t i = 0; i < n - listSize; ++i) {
            push_back(initialValue);
        } 
        return;
    }

    iterator pos = begin();
    for (size_t i = 0; i < n; ++i, ++pos);
    erase(pos, end());
}

template <typename T>
void
List<T>::push_back(const T& elem)
{
    end_ = new Node(elem, NULL, end_);
    if (NULL != end_->prev_) {
        end_->prev_->next_ = end_;
    }

    if (begin_ == NULL) {
        begin_ = end_; 
    }
}

template <typename T>
void
List<T>::push_front(const T& elem)
{
    begin_ = new Node(elem, begin_);
    if (begin_->next_ != NULL) {
        begin_->next_->prev_ = begin_;
    } else {
        end_ = begin_;
    }
}

template <typename T>
void
List<T>::pop_back()
{
    assert(begin_ != NULL);
    erase(end());
}

template <typename T>
void
List<T>::pop_front()
{
    assert(begin_ != NULL);
    erase(begin());
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(List<T>::iterator pos, const T& arg)
{
    if (begin() == pos && NULL == begin_) {
        push_back(arg);
        return begin();
    }
    
    if (pos == end()) {
        push_back(arg);
        pos.ptr_ = end_;
        return pos;
    }

    pos.setNext(new Node(arg, pos.next()));
    if (pos.next()->next_ != NULL) {
        pos.next()->next_->prev_ = pos.next();
    }
    pos.ptr_ = pos.next();
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(List<T>::iterator pos)
{
    if (begin() == pos && NULL == begin_) {
        push_back(T());
        return begin();
    }
    
    if (pos == end()) {
        push_back(T());
        pos.ptr_ = end_;
        return pos;
    }

    pos.setNext(new Node(T(), pos.next(), pos.node()));
    if (pos.next()->next_ != NULL) {
        pos.next()->next_->prev_ = pos.next();
    }
    pos.ptr_ = pos.next();
    return pos;

}

template <typename T>
void
List<T>::insert_after(List<T>::iterator pos, const int n, const T& arg)
{
    for (int i = 0; i < n; ++i) {
        pos = insert_after(pos, arg);
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert_after(List<T>::iterator pos, InputIterator first, InputIterator last)
{
    for (; first != last; ++first) {
        pos = insert_after(pos, *first);
   }
}

template <typename T>
typename List<T>::iterator
List<T>::insert_before(List<T>::iterator pos, const T& arg)
{
    if (pos == begin()) {
        push_front(arg);
        return --pos;
    }

    pos.ptr_->prev_ = new Node(arg, pos.ptr_, pos.ptr_->prev_);
    pos.ptr_->prev_->prev_->next_ = pos.ptr_->prev_;
    return --pos;
}

template <typename T>
typename List<T>::iterator
List<T>::insert_before(List<T>::iterator pos)
{

    if (pos == begin()) {
        push_front(T());
        return --pos;
    }

    pos.ptr_->prev_ = new Node(T(), pos.ptr_, pos.ptr_->prev_);
    pos.ptr_->prev_->prev_->next_ = pos.ptr_->prev_;
    return --pos;
}

template <typename T>
typename List<T>::iterator
List<T>::insert_before(List<T>::iterator pos, const int n, const T& arg)
{
    for (int i = 0; i < n; ++i) {
        pos = insert_before(pos, arg);
    }
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos)
{
    if (end_ == begin_) {
        begin_ = end_ = NULL;
        delete pos.ptr_;
        return pos;
    }

    if (pos == begin()) {
        begin_ = begin_->next_;
        begin_->prev_ = NULL;
        delete pos.ptr_;
        return pos;
    }

    if (pos.ptr_ == end_) {
        end_ = end_->prev_;
        end_->next_ = NULL;
        delete pos.ptr_;
        return pos;
    }
    
    if (pos == end()) { 
        end_ = end_->prev_;
        delete end_->next_;
        end_->next_ = NULL;
        return pos;
    }

    pos.ptr_->prev_->next_ = pos.ptr_->next_;
    pos.ptr_->next_->prev_ = pos.ptr_->prev_;
    delete pos.ptr_;
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator first, iterator last)
{
    if (last == first) {
        return first;
    }

    if (first == begin()) {
        begin_ = last.ptr_;
        if (begin_ != NULL) {
            begin_->prev_ = NULL;
        }
    } else {
        first.ptr_->prev_->next_ = last.ptr_;
        if (last.ptr_ != NULL) {
            last.ptr_->prev_ = first.ptr_->prev_;
        }
    }

    do {
        iterator tmp = first++;
        delete tmp.ptr_;
    } while (first != last);

    return first;
}

/// List<T>::const_iterator

template <typename T>
List<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
List<T>::const_iterator::const_iterator(List<T>::pointer ptr)
    : ptr_(ptr)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(const List<T>::const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
void
List<T>::const_iterator::setNext(pointer node)
{
    ptr_->next_ = node;
}

template <typename T>
const typename List<T>::value_type&
List<T>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator=(const const_iterator& rhv)
{
    if (ptr_ == rhv.ptr_) {
        return *this;
    }
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const typename List<T>::value_type* 
List<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
typename List<T>::const_iterator&
List<T>::const_iterator::operator++()
{ 
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_);
    ptr_ = ptr_->next_;
    return tmp;
}

template <typename T>
typename List<T>::const_iterator&
List<T>::const_iterator::operator--()
{ 
    ptr_ = ptr_->prev_;
    return *this;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator--(int)
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_);
    ptr_ = ptr_->prev__;
    return tmp;
}


template <typename T>
bool
List<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename T>
typename List<T>::pointer
List<T>::const_iterator::node() const
{
    return ptr_;
}

template <typename T>
void
List<T>::const_iterator::setPtr(const pointer ptr)
{
    ptr_ = ptr;
}

template <typename T>
typename List<T>::pointer
List<T>::const_iterator::next() 
{
    return ptr_->next_;
}

template <typename T>
typename List<T>::pointer
List<T>::const_iterator::prev() 
{
    return ptr_->prev_;
}

/// List<T>::iterator

template <typename T>
List<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
List<T>::iterator::iterator(const pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
List<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
List<T>::iterator::~iterator()
{
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator();
}

template <typename T>
typename List<T>::value_type*
List<T>::iterator::operator->() const
{
    return const_iterator::node()->data_;
}

template <typename T>
typename List<T>::iterator&
List<T>::iterator::operator=(const iterator& rhv)
{ 
    const_iterator::operator=(rhv);
    return *this;
}

template <typename T>
typename List<T>::value_type&
List<T>::iterator::operator*() const
{
    return const_iterator::node()->data_;
}

template <typename T>
typename List<T>::iterator&
List<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::node()->next_);
    return *this;
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator++(int)
{
    const iterator tmp(const_iterator::node());
    const_iterator::operator++();
    return tmp;
}

template <typename T>
typename List<T>::iterator&
List<T>::iterator::operator--()
{
    const_iterator::setPtr(const_iterator::node()->prev_);
    return *this;
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator--(int)
{
    const iterator tmp(const_iterator::node());
    const_iterator::operator--();
    return tmp;
}


/// const_reverse_iterator

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator()
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    const_reverse_iterator tmp(end_);
    return tmp;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    const_reverse_iterator tmp(begin_);
    return tmp;
}

template <typename T>
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator++()
{
    const_iterator::operator--();
    return *this;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(int)
{
    const const_reverse_iterator tmp(const_iterator::ptr_);
    const_iterator::operator--();
    return tmp;
}

template <typename T>
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator--()
{
    const_iterator::operator++();
    return *this;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(int)
{
    const const_reverse_iterator tmp(const_iterator::ptr_);
    const_iterator::operator++();
    return tmp;
}

template <typename T> 
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    if (const_iterator::ptr_ == rhv.ptr_) {
        return *this;
    }
    const_iterator::setPtr(rhv.node());
    return *this;
}

/// List<T>::reverse_iterator

template <typename T>
List<T>::reverse_iterator::reverse_iterator()
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(const pointer ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
List<T>::reverse_iterator::~reverse_iterator()
{
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{ 
    reverse_iterator tmp(end_);
    return tmp;
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    reverse_iterator tmp(begin_);
    return tmp;
}

template <typename T> 
typename List<T>::reverse_iterator&
List<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{

    if (iterator::node() == rhv.node()) {
        return *this;
    }
    const_iterator::setPtr(rhv.node());
    return *this;
}

template <typename T>
typename List<T>::reverse_iterator&
List<T>::reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::node()->prev_);
    return *this;
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator++(int)
{

    reverse_iterator tmp(const_iterator::node());
    const_iterator::operator--();
    return tmp;
}

template <typename T>
typename List<T>::reverse_iterator&
List<T>::reverse_iterator::operator--()
{
    const_iterator::operator++();
    return *this;
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::reverse_iterator::operator--(int)
{
    reverse_iterator tmp(const_iterator::node());
    const_iterator::operator++();
    return tmp;
}

template <typename T>
List<T>::Node::Node() 
    : data_(T())
    , next_(NULL)
    , prev_(NULL)
{
}

template <typename T>
List<T>::Node::Node(const T data, Node* next, Node* prev)
    : data_(data)
    , next_(next)
    , prev_(prev)
{
}

} /// end of namespace cd01


