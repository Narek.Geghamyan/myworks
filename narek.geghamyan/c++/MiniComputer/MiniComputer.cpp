#include <iostream>
#include <cassert>
#include <unistd.h>
#include <stdlib.h>

#include "MiniComputer.hpp"

MiniComputer::MiniComputer()
{
    isInteractive_ = ::isatty(STDIN_FILENO);
    programName_ = "Mini Computer";
    version_ = "Version 1.0";
}

int
MiniComputer::run()
{
    /// Print program name and version
    /// Print main menu
    /// Get command from user
    /// Execute the command
    /// Start again from main menu
    printProgramNameAndVersion();
    
    int command = -1;
    while (command != 0) {
        printMainMenu();
        command = getCommandFromUser("Command");
        executeCommand(command);
    }

    return 0;
}

inline void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << programName_ << " - " << version_ << std::endl;
}

void
MiniComputer::printMainMenu()
{
    if (isInteractive_) {
        std::cout << "Main Menu\n";
        for (int itemCount = 0; itemCount < 5; ++itemCount) {
            printMainMenuItem(itemCount);
        }
    }
}

void
MiniComputer::printLoadMenu()
{
    if (isInteractive_) {
        std::cout << "Load Menu\n";
        for (int itemCount = 0; itemCount < 4; ++itemCount) {
            printRegisterMenuItem(itemCount);
        }
    }
}

void
MiniComputer::printLoadIntoMenu(int registerNumber)
{
    if (isInteractive_) {
        const char registerName = getRegisterName(registerNumber);
        std::cout << "Load Into " << registerName << '\n';
        std::cout << "\tInput the value to load into register " << registerName << '\n';
    }
}

void
MiniComputer::printPrintMenu()
{
    if (isInteractive_) {
        std::cout << "Print Menu\n";
        for (int itemCount = 0; itemCount < 2; ++itemCount) {
            printPrintMenuItem(itemCount);
        }
    }
}

void
MiniComputer::printAddMenu()
{
    if (isInteractive_) {
        std::cout << "Add Menu (Register 1 + Register 2 = Register 3)\n";
        for (int itemCount = 0; itemCount < 4; ++itemCount) {
            printRegisterMenuItem(itemCount);
        }
    }
}

void
MiniComputer::printMainMenuItem(const int index)
{
    std::cout << '\t' << index << " - ";
    switch (index) {
    case 0: std::cout << "Exit";  break;
    case 1: std::cout << "Load";  break;
    case 2: std::cout << "Store"; break;
    case 3: std::cout << "Print"; break;
    case 4: std::cout << "Add";   break;
    default:
        std::cerr << "Assert 1: There are 5 main menu items." << std::endl;
        assert(0);
    }
    std::cout << '\n';
}

void
MiniComputer::printRegisterMenuItem(const int index)
{
    std::cout << '\t' << index << " - " << getRegisterName(index) << '\n';
}

void
MiniComputer::printPrintMenuItem(const int index)
{
    std::cout << '\t' << index << " - ";
    switch (index) {
    case 0: std::cout << "Register"; break;
    case 1: std::cout << "String";   break;
    default:
        std::cerr << "Assert 2: There are 2 print menu items." << std::endl;
        assert(0);
    }
    std::cout << '\n';
}

inline int
MiniComputer::getCommandFromUser(const std::string& prompt)
{
    if (isInteractive_) {
        std::cout << "> " << prompt << ": ";
    }

    int command;
    std::cin >> command;
    return command;
}

inline char
MiniComputer::getRegisterName(const int registerNumber)
{
    return 'a' + static_cast<char>(registerNumber);
}

void
MiniComputer::setRegisterValue(const int registerNumber, const int registerValue)
{
    switch (registerNumber) {
    case 0: registerA_ = registerValue; break;
    case 1: registerB_ = registerValue; break;
    case 2: registerC_ = registerValue; break;
    case 3: registerD_ = registerValue; break;
    default:
        std::cout << "Error 2: Invalid register" << std::endl;
        ::exit(2);
    }
}

int
MiniComputer::getRegisterValue(const int registerNumber)
{
    switch (registerNumber) {
    case 0: return registerA_;
    case 1: return registerB_;
    case 2: return registerC_;
    case 3: return registerD_;
    default:
        std::cout << "Error 2: Invalid register" << std::endl;
        ::exit(2);
    }
    assert(0);
    return 0;
}

void
MiniComputer::executeCommand(const int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executeStoreCommand(); break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    default:
        std::cout << "Error 1: Invalid command" << std::endl;
        ::exit(1);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
}

void
MiniComputer::executeLoadCommand()
{
    printLoadMenu();
    const int registerNumber = getCommandFromUser("Register");
    executeLoadIntoCommand(registerNumber);
}

void
MiniComputer::executeLoadIntoCommand(int registerNumber)
{
    printLoadIntoMenu(registerNumber);
    std::string registerName = std::string(1, getRegisterName(registerNumber));
    const int registerValue = getCommandFromUser(registerName);
    executeLoadIntoRegisterCommand(registerNumber, registerValue);
}

void
MiniComputer::executeLoadIntoRegisterCommand(int registerNumber, int registerValue)
{
    setRegisterValue(registerNumber, registerValue);
    std::cout << getRegisterName(registerNumber) << " = "
              << getRegisterValue(registerNumber) << std::endl;
}

void
MiniComputer::executeStoreCommand()
{
    assert(0);
}

void
MiniComputer::executePrintCommand()
{
    printPrintMenu();
    const int print = getCommandFromUser("Print");
    (0 == print) ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executePrintRegister()
{
    assert(0);
}


void
MiniComputer::executePrintString()
{
    assert(0);
}

void
MiniComputer::executeAddCommand()
{
    printAddMenu();

    const int register1Number = getCommandFromUser("Register 1");
    const int register2Number = getCommandFromUser("Register 2");
    const int register3Number = getCommandFromUser("Register 3");

    executeAdd(register1Number, register2Number, register3Number);
}

void
MiniComputer::executeAdd(const int register1Number, const int register2Number, const int register3Number)
{
    const char register1Name = getRegisterName(register1Number);
    const int register1Value = getRegisterValue(register1Number);
    const char register2Name = getRegisterName(register2Number);
    const int register2Value = getRegisterValue(register2Number);
    const int sumOfRegisterValues = register1Value + register2Value;
    const char register3Name = getRegisterName(register3Number);
    setRegisterValue(register3Number, sumOfRegisterValues);
    const int register3Value = getRegisterValue(register3Number);

    std::cout << register3Name  << " = " << register1Name  << " + "
              << register2Name  << " = " << register1Value << " + "
              << register2Value << " = " << register3Value << std::endl;

}

