#include <string>

class MiniComputer
{
public:
    MiniComputer();
    int run();

private:
    void printProgramNameAndVersion();
    void printMainMenu();
    void printLoadMenu();
    void printLoadIntoMenu(const int registerNumber);
    void printPrintMenu();
    void printAddMenu();
    void printMainMenuItem(const int index);
    void printRegisterMenuItem(const int index);
    void printPrintMenuItem(const int index);
    int getCommandFromUser(const std::string& prompt);
    char getRegisterName(const int registerNumber);
    void setRegisterValue(const int registerNumber, const int registerValue);
    int getRegisterValue(const int registerNumber);

    void executeCommand(const int command);
    void executeExitCommand();
    void executeLoadCommand();
    void executeLoadIntoCommand(const int registerNumber);
    void executeLoadIntoRegisterCommand(const int registerNumber, const int registerValue);
    void executeStoreCommand();
    void executePrintCommand();
    void executePrintRegister();
    void executePrintString();
    void executeAddCommand();
    void executeAdd(const int register1Number, const int register2Number, const int register3Number);

private:
    bool isInteractive_;
    std::string programName_;
    std::string version_;
    int registerA_;
    int registerB_;
    int registerC_;
    int registerD_;
};
