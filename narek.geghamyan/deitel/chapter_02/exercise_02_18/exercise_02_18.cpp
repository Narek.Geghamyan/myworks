#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Input two numbers: ";
    std::cin >> number1 >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is more than " << number2 << std::endl;
        return 0;
    }
    
    if (number1 < number2) {
        std::cout << number1 << " is less than " << number2 << std::endl;
        return 0;
    }

    std::cout << "These numbers are equal" << std::endl;
    
    return 0;
}
