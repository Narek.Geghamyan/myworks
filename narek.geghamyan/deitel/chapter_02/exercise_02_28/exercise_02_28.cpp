#include <iostream>

int 
main()
{
    int number;
    std::cout << "Input a five-digit number: ";
    std::cin >> number;
    int firstDigit = number / 10000;
   
    if (0 == firstDigit) {
        std::cout << "Error 1: Wrong number" << std::endl;
        return 1;
    }
    
    if (9 < firstDigit) { 
        std::cout << "Error 1: Wrong number" << std::endl;
        return 1;
    }

    std::cout << firstDigit  << " " << number / 1000 % 10  << " " << number / 100 % 10  << " " << number / 10 % 10  << " " << number  % 10 << std::endl;

    return 0;
}
