#include <iostream>

int
main() 
{
    std::cout << "ASCII A is: " << static_cast <int> ('A') << std::endl;
    std::cout << "ASCII B is: " << static_cast <int> ('B') << std::endl;
    std::cout << "ASCII C is: " << static_cast <int> ('C') << std::endl;
    std::cout << "ASCII a is: " << static_cast <int> ('a') << std::endl;
    std::cout << "ASCII b is: " << static_cast <int> ('b') << std::endl;
    std::cout << "ASCII c is: " << static_cast <int> ('c') << std::endl;
    std::cout << "ASCII 0 is: " << static_cast <int> ('0') << std::endl;
    std::cout << "ASCII 1 is: " << static_cast <int> ('1') << std::endl;
    std::cout << "ASCII 2 is: " << static_cast <int> ('2') << std::endl;
    std::cout << "ASCII $ is: " << static_cast <int> ('$') << std::endl;
    std::cout << "ASCII * is: " << static_cast <int> ('*') << std::endl;
    std::cout << "ASCII + is: " << static_cast <int> ('+') << std::endl;
    std::cout << "ASCII / is: " << static_cast <int> ('/') << std::endl;
    std::cout << "ASCII space is: " << static_cast <int> (' ') << std::endl;

    return 0;
}
