#include <iostream>

int
main()
{
   int number1, number2;
   std::cout << "Input two numbers: ";
   std::cin >> number1 >> number2;

   if (0 == number2) {
       std::cout << "Error 1: Division by zero." << std::endl;
       return 1;
   }
   
   std::cout << number1 << " + " << number2 << " = " << number1 + number2 << std::endl;
   std::cout << number1 << " - " << number2 << " = " << number1 - number2 << std::endl;
   std::cout << number1 << " * " << number2 << " = " << number1 * number2 << std::endl;
   std::cout << number1 << " / " << number2 << " = " << number1 / number2 << std::endl;
   std::cout << number1 << " % " << number2 << " = " << number1 % number2 << std::endl;
    
   return 0;
}
