Fill in the blanks in each of the following:

a. ______ are used to docum ent a program and im prove its readability. (comments)

b. The object used to print inform ation on the screen is _____. (cout)

c. A C++ statem ent that m ak es a decision is ______. (conditional statements) (if/else)

d. Most calculations are norm ally perform ed by ______ statem ents. (operator computation + - / * %)

e. The ______ object inputs values from the k eyboard. (cin) (input)
