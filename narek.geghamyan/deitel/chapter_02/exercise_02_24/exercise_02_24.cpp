#include <iostream>

int
main()
{
    int number;
    std::cout << "Input number: ";
    std::cin >> number;

    if (number % 2 == 0) {
        std::cout << "This number is even" << std::endl;
        return 0;
    }

    std::cout << "This number is not even" << std::endl;
    
    return 0;
}
