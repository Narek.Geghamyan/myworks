#include <iostream>
   
int
main()
{
    int number1, number2, number3;
    std::cout << "Input three numbers: ";
    std::cin >> number1 >> number2 >> number3;
    int maximum = number1;
    int minimum = number2;
  
    if (number2 > maximum) {
        maximum = number2;
    }
  
    if (number3 > maximum) {
        maximum = number3;
    }
  
    if (number2 < minimum) {
        minimum = number2;
    }
  
    if (number3 <minimum) {
        minimum = number3;
    }
  
    std::cout << "The sum is: " << number1 + number2 + number3 << std::endl;
    std::cout << "The average is: " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "The product is: " << number1 * number2 * number3 << std::endl;
    std::cout << "The smallest is: " << minimum << std::endl;
    std::cout << "The largest is: " << maximum << std::endl;
 
    return 0;
}
