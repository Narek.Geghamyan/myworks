#ifndef __STRING_HPP__
#define __STRING_HPP__

#include <iostream>

class String
{
    friend std::ostream& operator<<(std::ostream& output, const String& s) ;
    friend std::istream& operator>>(std::istream& input, String& s) ;
public:
    String(const char* s = NULL);
    String(const String& copy);
    ~String();
    const String& operator=(const String& rhv);
    const String& operator+=(const String& rhv);
    const String& operator+(const String& rhv);
    bool operator!() const;
    bool operator==(const String& rhv) const;
    bool operator<(const String& rhv) const;
    bool operator!=(const String& rhv) const;
    bool operator>(const String& rhv) const;
    bool operator<=(const String& rhv) const;
    bool operator>=(const String& rhv) const;
    char& operator[](const int subscript);
    char operator[](const int subscript) const;
    const String operator()(int index, int sublength = 0) const;
    int getLength() const;
private:
    void setString(const char* str);
private:
    int length_;
    char *sPtr_;
};

#endif /// __STRING_HPP__

