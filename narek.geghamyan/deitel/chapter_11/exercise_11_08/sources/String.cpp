#include <iostream>
#include <iomanip>
#include <cstring>
#include "headers/String.hpp"

String::String(const char* s)
    : length_((s != NULL) ? strlen(s) : 0)
{
    setString(s);
}

String::String(const String& copy)
    : length_(copy.length_)
{
    setString(copy.sPtr_);
}

String::~String()
{
    delete [] sPtr_;
}

const String&
String::operator=(const String& rhv)
{
    if (&rhv != this) {
        delete [] sPtr_;
        length_ = rhv.length_;
        setString(rhv.sPtr_);
    } else {
        std::cerr << "Attempted assignment of a String to itself" << std::endl;
    }
    return *this;
}

const String&
String::operator+=(const String& rhv)
{
    size_t newlength_ = length_ + rhv.length_;
    char *tempPtr = new char[newlength_ + 1];

    ::strcpy(tempPtr, sPtr_);
    ::strcpy(tempPtr + length_, rhv.sPtr_);

    delete [] sPtr_;
    sPtr_ = tempPtr; 
    length_ = newlength_;
    return *this;
}

bool
String::operator!() const
{
    return length_ == 0;
}

bool
String::operator==(const String& rhv) const
{
    return ::strcmp(sPtr_, rhv.sPtr_) == 0;
}

bool
String::operator<(const String& rhv) const
{
    return ::strcmp(sPtr_, rhv.sPtr_) < 0;
}

const String&
String::operator+(const String& rhv)
{
    const size_t newlength_ = length_ + rhv.length_;

    char *tempPtr = new char[newlength_ + 1];
    ::strcpy(tempPtr, sPtr_);
    ::strcpy(tempPtr + length_, rhv.sPtr_);

    delete [] sPtr_;
    sPtr_ = tempPtr; 
    length_ = newlength_;
    return *this;
}

char&
String::operator[](const int subscript)
{
    if (subscript < 0 || subscript >= length_) {
        std::cerr << "Error: Subscript " << subscript << " out of range" << std::endl;
        exit(1);
    }
    return sPtr_[subscript];
}

char 
String::operator[](const int subscript) const
{
    if (subscript< 0 || subscript >= length_) {
        std::cerr << "Error: Subscript " << subscript << " out of range" << std::endl;
        exit(1);
    }
    return sPtr_[subscript];
}

const String
String::operator()(int index, int sublength) const
{

    if (index < 0 || index >= length_ || sublength < 0) {
        return "";
    }

    int len;
    if ((sublength == 0) || (index + sublength > length_)) {
        len = length_ - index;
    } else {
        len = sublength;
    }
    char *tempPtr = new char[len + 1];
    ::strncpy(tempPtr, &sPtr_[index], len);
    tempPtr[len] = '\0';

    String tempString(tempPtr);
    delete [] tempPtr;
    return tempString;
}

int
String::getLength() const
{
    return length_;
}

void
String::setString(const char* str)
{
    sPtr_ = new char[length_ + 1];
    if (str != NULL) {
        ::strcpy(sPtr_, str);
    } else {
        sPtr_[0] = '\0';
    }
}

std::ostream&
operator<<(std::ostream& output, const String& s)
{
    output << s.sPtr_;
    return output;
}

std::istream&
operator>>(std::istream& input, String& s)
{
    char temp[100];
    input >> std::setw(100) >> temp;
    s = temp;
    return input;
}

