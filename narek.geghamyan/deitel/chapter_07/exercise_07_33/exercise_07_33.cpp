#include <iostream>
#include <unistd.h>

int linearSearch(const int integersArray[], const int index, const int arraySize, const int key);
void inputElements(int integersArray[], const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 10;
    int integersArray[ARRAY_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "input array elements: ";
    }
    inputElements(integersArray, ARRAY_SIZE);
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input key: ";
    }
    int key;
    std::cin >> key;
    const int element = linearSearch(integersArray, 0, ARRAY_SIZE, key);
    if (-1 == element) {
        std::cout << "The number you are looking for was not found." << std::endl;
        return 0;
    }

    std::cout << "The index of the element to be searched for in the array: " << element << std::endl;
    return 0;
}

void
inputElements(int integersArray[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cin >> integersArray[i];
    }
}

int
linearSearch(const int integersArray[], const int index, const int arraySize, const int key)
{
    if (integersArray[index] == key) {
        return index;
    }

    if (index == arraySize) {
        return -1;
    }

    return linearSearch(integersArray, index + 1, arraySize, key);
}
