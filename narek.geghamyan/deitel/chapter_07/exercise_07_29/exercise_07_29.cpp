#include <iostream>
#include <cmath>

void searchPrime(int array[], const int arraySize);
void printArray(const int array[], const int arraySize);
void setElements(int array[], const int arraySize);
void printPrimeIndex(const int array[], const int arraySize);

int
main()
{

    const int ARRAY_SIZE = 1000;
    int array[ARRAY_SIZE] = {};
    setElements(array, ARRAY_SIZE);

    std::cout << "Original array: ";
    printArray(array, ARRAY_SIZE);
    searchPrime(array, ARRAY_SIZE);
    std::cout << "Array after separating primes (1 - prime): ";
    printArray(array, ARRAY_SIZE);
    std::cout << "Prime numbers indexes: ";
    printPrimeIndex(array, ARRAY_SIZE);
    return 0;
}

void
searchPrime(int array[], const int arraySize)
{
    array[0] = 0;
    for (int i = 2; i < arraySize / 2; ++i) {
        if (1 == array[i]) {
            for (int j = i + i; j < arraySize; j += i) {
                array[j] = 0;
            }
        }
    }
}

void
printArray(const int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << "\n\n";
}

void
setElements(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        array[i] = 1;
    }
}

void
printPrimeIndex(const int array[], const int arraySize)
{
    for (int i = 1; i < arraySize; ++i) {
        if (1 == array[i]) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
}

