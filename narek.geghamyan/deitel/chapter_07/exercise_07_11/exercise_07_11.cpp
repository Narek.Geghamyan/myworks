#include <iostream>
#include <unistd.h>

void bubbleSort(int integersArray[], const int size);
void printArray(const int integersArray[], const int size);

int
main()
{
    const int SIZE = 10;
    int integersArray[SIZE] = {0};
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input ten numbers: ";
    }

    for (int i = 0; i < SIZE; ++i) {
        std::cin >> integersArray[i];
    }
    bubbleSort(integersArray, SIZE);
    printArray(integersArray, SIZE);
    return 0;
}

void
bubbleSort(int integersArray[], const int size)
{
    for (int i = 0; i < size; ++i) {
        for (int k = 1; k < size; ++k) {
            if (integersArray[k] < integersArray[k - 1]) {
                const int swap = integersArray[k];
                integersArray[k] = integersArray[k - 1];
                integersArray[k - 1] = swap;
            }
        }
    }
}

void
printArray(const int integersArray[], const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << integersArray[i] << " ";
    }
    std::cout << std::endl;
}

