#include <iostream>
#include <unistd.h>

void setArrayElements(int array[], const int arraySize);
void selectionSort(int array[], const int index, const int arraySize);
void printArray(const int array[], const int arraySize);
int getMinimumIndex(const int array[], const int index, const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    setArrayElements(array, ARRAY_SIZE);
    selectionSort(array, 0, ARRAY_SIZE);
    printArray(array, ARRAY_SIZE);
    return 0;
}

void
setArrayElements(int array[], const int arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << arraySize << " numbers: ";
    }

    for (int i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

void
selectionSort(int array[], const int index, const int arraySize)
{
    if (array[index] == array[arraySize - 1]) {
        return;
    }

    const int minIndex = getMinimumIndex(array, index, arraySize);
    std::swap(array[index], array[minIndex]);
    selectionSort(array, index + 1, arraySize);
}

void
printArray(const int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

int
getMinimumIndex(const int array[], const int index, const int arraySize)
{
    int minIndex = arraySize;
    for (int i = index; i < arraySize; ++i) {
        if (array[i] < array[minIndex]) {
             minIndex = i;
        }
    }
    return minIndex;
}

