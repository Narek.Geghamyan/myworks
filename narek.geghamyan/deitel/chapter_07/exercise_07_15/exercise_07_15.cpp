#include <iostream>
#include <stdlib.h>
#include <unistd.h>

inline bool
checking(const int array[], const int counter, const int number)
{
    for (int i = 0; i < counter; ++i) {
        if (number == array[i]) {
            return true;
        }
    }
    return false;
}

int
main()
{
    ::srand(::time(0));
    const int SIZE = 19;
    int array[SIZE] = {};
    int counter = 0;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input ten numbers: ";
    }

    for (int i = 0; i < 20; ++i) {
        int number;
        std::cin >> number;
        if (number < 10 || number > 100) {
            std::cerr << "Error 1: Your number is out of range.\a" << std::endl;
        }

        if(!(checking(array, i, number))) {
            std::cout << number << " ";
            if (counter < SIZE) {
                array[counter++] = number;
            }
        }
    }
    std::cout << std::endl;
    return 0;
}

