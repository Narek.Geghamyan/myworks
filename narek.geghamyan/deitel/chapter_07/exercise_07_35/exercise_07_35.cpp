#include <iostream>
#include <unistd.h>

void printArray(const int array[], const int counter, const int arraySize);
void inputElements(int array[], const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array elements: ";
    }

    inputElements(array, ARRAY_SIZE);
    printArray(array, 0, ARRAY_SIZE);
    return 0;
}

void
inputElements(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

void
printArray(const int array[], const int counter, const int arraySize)
{
    if (0 == arraySize) {
        return;
    }

    if (counter == arraySize) {
        std::cout << std::endl;
        return;
    }
    std::cout << array[counter] << " ";
    printArray(array, counter + 1, arraySize);
}

