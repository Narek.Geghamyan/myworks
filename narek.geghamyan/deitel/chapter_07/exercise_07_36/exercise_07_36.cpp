#include <iostream>
#include <cstring>
#include <unistd.h>

void stringReverse(const char stringArray[], const int index, const int stringSize);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input string: ";
    }

    const int ARRAY_SIZE = 256;
    char stringArray[ARRAY_SIZE];
    std::cin.getline(stringArray, ARRAY_SIZE);
    const int arraySize = ::strlen(stringArray);
    stringReverse(stringArray, 0, arraySize);
    return 0;
}

void
stringReverse(const char stringArray[], const int index, const int stringSize)
{
    if (index == stringSize) {
        std::cout << std::endl;
        return;
    }

    std::cout << stringArray[stringSize - 1 - index];
    stringReverse(stringArray, index + 1, stringSize);
}

