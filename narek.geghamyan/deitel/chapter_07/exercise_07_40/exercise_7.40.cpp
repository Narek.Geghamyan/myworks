#include <iostream>
#include <unistd.h>
#include <vector>

int recursiveMinimum(const std::vector<int>& numbersVector, const int index, const int vectorSize, int minimumIndex);
void inputElements(std::vector<int>& numbersVector, const int vectorSize);

int
main()
{
    const int VECTOR_SIZE = 10;
    std::vector<int> numbersVector (VECTOR_SIZE);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << VECTOR_SIZE << " numbers: ";
    }
    inputElements(numbersVector, VECTOR_SIZE);

    const int minimum = recursiveMinimum(numbersVector, 0, VECTOR_SIZE, 0);
    std::cout << "The smallest element: " << minimum << std::endl;
    return 0;
}

void
inputElements(std::vector<int>& numbersVector, const int vectorSize)
{
    for (int i = 0; i < vectorSize; ++i) {
        std::cin >> numbersVector[i];
    }
}

int
recursiveMinimum(const std::vector<int>& numbersVector, const int index, const int vectorSize, int minimumIndex)
{
    if (vectorSize <= 1) {
        return numbersVector[0];
    }

    if (index == vectorSize) {
        return numbersVector[minimumIndex];
    }

    if (numbersVector[index] < numbersVector[minimumIndex]) {
        minimumIndex = index;
    }
    return recursiveMinimum(numbersVector, index + 1, vectorSize, minimumIndex);
}

