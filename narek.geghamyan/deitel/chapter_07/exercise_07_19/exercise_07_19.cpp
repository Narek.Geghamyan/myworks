#include <iostream>
#include <cstdlib>
#include <cassert>
#include <unistd.h>
#include <iomanip>

int rollDice();
void playCraps(int losingArray[], int winArray[], const int size);
void printResults(const int losingArray[], const int winArray[], const int size);
void printGameCounter(const int gameCounter);
float oddsOfWinning(const int winArray[], const int size, const int numberOfGames);
float averageDuration(const int winArray[], const int losingArray[], const int size);

int
main()
{
    if ((::isatty(STDIN_FILENO))) {
        ::srand(::time(0));
    }

    const int SIZE = 21;
    int losingArray[SIZE] = {};
    int winArray[SIZE] = {};
    const int NUMBER_OF_GAMES = 1000;
    for (int counter = 0; counter < NUMBER_OF_GAMES; ++counter) {
        playCraps(losingArray, winArray, SIZE);
    }
    printResults(losingArray, winArray, SIZE);
    std::cout << "Odds of winning: " << (oddsOfWinning(winArray, SIZE, NUMBER_OF_GAMES)) * 100 << "%" << std::endl;

    const float averageSum = averageDuration(winArray, losingArray, SIZE);
    std::cout << "Average game duration: " << std::setprecision(3) << averageSum / 1000 << std::endl;
    std::cout << "Do the odds of winning increase with the length of the game?: " << "No" << std::endl;
    return 0;
}

int
rollDices()
{
    const int dice1 = 1 + ::rand() % 6;
    const int dice2 = 1 + ::rand() % 6;
    const int sum = dice1 + dice2;
    return sum;
}

void
playCraps(int losingArray[], int winArray[], const int size)
{
    enum Status {CONTINUE, WON, LOST};
    Status gameStatus;
    int myPoint = 0;
    const int sumOfDice = rollDices();
    switch (sumOfDice) {
    case 7:
    case 11: gameStatus = WON; ++winArray[0];     break;
    case 2:
    case 3:
    case 12: gameStatus = LOST; ++losingArray[0]; break;
    default:
        myPoint = sumOfDice;
        gameStatus = CONTINUE;
        break;
    }

    int counter = 0;
    while (CONTINUE == gameStatus) {
        const int sumOfDice = rollDices();
        if (counter < size) {
            ++counter;      
        } 
        
        if (counter >= size) {
            counter = size - 1;
        }

        if (sumOfDice == myPoint) {
            gameStatus = WON;
            ++winArray[counter];
        } else if (7 == sumOfDice) {
            gameStatus = LOST;
            ++losingArray[counter];
        }
    }
}

void
printResults(const int losingArray[], const int winArray[], const int size)
{
    for (int i = 1; i < size; ++i) {
        std::cout << "Games finished in " << i << " attempt | Win: " << winArray[i - 1] 
                  << " | Losing: " << losingArray[i - 1] << std::endl;
    }
    std::cout << "Game over with more than 20 tries| Win: " << winArray[size - 1] 
              << " | Losing: " << losingArray[size - 1] << std::endl;
}

void
printGameCounter(const int gameCounter)
{
    std::cout << "Average game duration: " << gameCounter << std::endl;
}

float
oddsOfWinning(const int winArray[], const int size, const int numberOfGames)
{
    float sum = 0.0;
    for (int i = 0; i < size; ++i) {
        sum += winArray[i];
    }

    const float total = sum / static_cast<float>(numberOfGames);
    return total;
}

float
averageDuration(const int winArray[], const int losingArray[], const int size)
{
    float averageSum = 0;
    for (int i = 1; i < size; ++i) {
        averageSum += (winArray[i - 1] + losingArray[i - 1]) * i;
    }
    return averageSum;
}

