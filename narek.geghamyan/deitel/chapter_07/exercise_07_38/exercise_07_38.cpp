#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <vector>

void inputSalaries(std::vector<int>& counterVector , const int counterSize);
void printRanges(std::vector<int>& counterVector, const int counterSize);

int
main()
{
    const int COUNTER_SIZE = 9;
    std::vector<int> counterVector(COUNTER_SIZE);
    inputSalaries(counterVector, COUNTER_SIZE);
    printRanges(counterVector, COUNTER_SIZE); 
    return 0;
}

void
inputSalaries(std::vector<int>& counterVector, const int counterSize)
{
    for (int i = 0; i < counterSize; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input sales: ";
        }

        int sale;
        std::cin >> sale;
        if (sale < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            ::exit(1);
        }

        const int salary = static_cast<int>(200 + sale * 0.09);
        const int counter = salary / 100 - 2;
        if (counter > 8) {
            ++counterVector[8];
        } else { 
            ++counterVector[counter];
        }
    }
}

void
printRanges(std::vector<int>& counterVector, const int counterSize)
{
    int counter = 0;
    for (int i = 200; ; i += 100) {
        if (i >= 1000) {
            std::cout << "$1000 and more: " << counterVector[counterSize - 1] << std::endl;
            break;
        }

        std::cout << "$" << i << " - " << i + 99 << ": ";
        std::cout << counterVector[counter] << std::endl;
        ++counter;
    }
}

