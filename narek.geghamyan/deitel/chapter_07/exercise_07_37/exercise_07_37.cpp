#include <iostream>
#include <unistd.h>

int recursiveMinimum(const int array[], const int index, const int arraySize, int minimumIndex);
void inputElements(int array[], const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << ARRAY_SIZE << " numbers: ";
    }

    inputElements(array, ARRAY_SIZE);
    const int minimum = recursiveMinimum(array, 0, ARRAY_SIZE, 0);
    std::cout << "The smallest element: " << minimum << std::endl;
    return 0;
}

void
inputElements(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

int
recursiveMinimum(const int array[], const int index, const int arraySize, int minimumIndex)
{
    if (arraySize <= 1) {
        return array[0];
    }

    if (index == arraySize) {
        return array[minimumIndex];
    }

    if (array[index] < array[minimumIndex]) {
        minimumIndex = index;
    }
    return recursiveMinimum(array, index + 1, arraySize, minimumIndex);
}

