#include <iostream>
#include "Date.hpp"

Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void
Date::setDay(int day)
{
    day_ = day;

    if (31 < day) {
        std::cout << "Info 1: Day cannot be more than 31. "
                  << "Day is set to 1" << std::endl;        
        day_ = 1;       
    }

    if (0 >= day) {
        std::cout << "Info 1: A day cannot be zero and cannot be negative. "
                  << "Day is set to 1" << std::endl;
        day_ = 1;
    }
}

void
Date::setMonth(int month)
{
    month_ = month;

    if (12 < month) {
        std::cout << "Info 1: Month connot be more than 12. "
                  << "Month is set to 1" << std::endl;
        month_ = 1;
    }

    if (0 >= month) {
        std::cout << "Info 1: A month connot be zero and connot be negative. "
                  << "Month is set to 1" << std::endl;
        month_ = 1;
    }
}

void 
Date::setYear(int year)
{
    year_ = year;

    if (0 >= year) {
        std::cout << "Info 1: Year cannot be negative. "
                  << "Year set for 2020" << std::endl;
        year_ = 2020;
    }
}

int
Date::getDay()
{
    return day_;
}

int
Date::getMonth()
{
    return month_;
}

int
Date::getYear()
{
    return year_;
}

void
Date::displayDate()
{
    std::cout << "Today's date: " << Date::getDay() << " / " << Date::getMonth() << " / " << Date::getYear() << std::endl;
}
