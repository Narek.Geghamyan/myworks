class Date
{
public:
    Date(int day, int month, int year);
    void setMonth(int month);
    void setDay(int day);
    void setYear(int year);
    int getMonth();
    int getDay();
    int getYear();
    void displayDate();
private:
    int day_;
    int month_;
    int year_;
};
