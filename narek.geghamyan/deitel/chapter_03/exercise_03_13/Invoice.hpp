class Invoice
{
public:
    Invoice(std::string partNumber, std::string descriptionOfItem, int quantityOfItem, int pricePerItem);
    std::string  getPartNumber();
    void setPartNumber(std::string partNumber);
    std::string getDescriptionOfItem();
    void setDescriptionOfItem(std::string descriptionOfItem);
    int getQuantityOfItem();
    void setQuantityOfItem(int quantityOfItem);
    int getPricePerItem();
    void setPricePerItem(int pricePerItem);
    int getInvoiceAmount();
private:
    std::string partNumber_;
    std::string descriptionOfItem_;
    int quantityOfItem_;
    int pricePerItem_;
};
