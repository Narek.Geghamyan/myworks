#include <iostream>
#include "Invoice.hpp"

Invoice::Invoice(std::string partNumber, std::string descriptionOfItem, int quantityOfItem, int pricePerItem)
{
    setPartNumber(partNumber);
    setDescriptionOfItem(descriptionOfItem);
    setQuantityOfItem(quantityOfItem);
    setPricePerItem(pricePerItem);
}

void 
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

void 
Invoice::setDescriptionOfItem(std::string descriptionOfItem)
{
    descriptionOfItem_ = descriptionOfItem;
}

void
Invoice::setQuantityOfItem(int quantityOfItem)
{
    quantityOfItem_ = quantityOfItem;

    if (quantityOfItem < 0) {
        std::cout << "Given Quantity Of Item is negative. Resetting to 0..." << std::endl;
        quantityOfItem_ = 0;
    }
}

void
Invoice::setPricePerItem(int pricePerItem)
{
    pricePerItem_ = pricePerItem;

    if (pricePerItem < 0) {
        std::cout << "Given price of item is negative. Resetting to 0..." << std::endl;
        pricePerItem_ = 0;        
    }
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

std::string 
Invoice::getDescriptionOfItem()
{
    return descriptionOfItem_;
}

int
Invoice::getQuantityOfItem()
{
    return quantityOfItem_;
}

int
Invoice::getPricePerItem()
{
    return pricePerItem_;
}
    
int
Invoice::getInvoiceAmount()
{   
    return pricePerItem_ * quantityOfItem_;
}
