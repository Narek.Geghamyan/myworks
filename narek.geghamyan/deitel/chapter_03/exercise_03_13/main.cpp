#include <iostream>
#include "Invoice.hpp"

int
main()
{
    Invoice item("a76", "flash drive", 20, 50);

    std::cout << "\tProduct code: " << item.getPartNumber() << std::endl;
    std::cout << "\tProduct description: " << item.getDescriptionOfItem() << std::endl;
    std::cout << "\tPumber of goods sold: " << item.getQuantityOfItem() << std::endl; 
    std::cout << "\tPrice of one item: " << item.getPricePerItem() << std::endl;
    std::cout << "\tTotal invoice amount: " << item.getInvoiceAmount() << std::endl;

    return 0;
}
