#include <iostream>
#include "Employee.hpp"

Employee::Employee(std::string name, std::string surName, int monthlySalary)
{
    setName(name);
    setSurName(surName);
    setMonthlySalary(monthlySalary);
}

void
Employee::setName(std::string name)
{
    name_ = name;
}

void
Employee::setSurName(std::string surName)
{
    surName_ = surName;
}

void
Employee::setMonthlySalary(int monthlySalary)
{
    monthlySalary_ = monthlySalary;

    if (0 > monthlySalary) {
        std::cout << "Info 1: Given salary is negative. Resetting to 0..." << std::endl;
        monthlySalary_ = 0;
    }
}

std::string
Employee::getName()
{
    return name_;
}

std::string
Employee::getSurName()
{
    return surName_;
}

int
Employee::getMonthlySalary()
{
    return monthlySalary_;
}

