class Employee
{
public:
    Employee(std::string name, std::string surName, int monthlySalary);
    void setName(std::string name);
    void setSurName(std::string surName);
    void setMonthlySalary(int monthlySalary);
    std::string getName();
    std::string getSurName();
    int getMonthlySalary();
private:
    std::string name_;
    std::string surName_;
    int monthlySalary_;
};
