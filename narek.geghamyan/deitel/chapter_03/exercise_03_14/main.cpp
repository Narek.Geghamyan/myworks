#include <iostream>
#include "Employee.hpp"

int main()
{    
    Employee employee1("Vazgen", "Hunanyan", 150000);
    int monthSalary1 = employee1.getMonthlySalary();

    std::cout << employee1.getName() << " " << employee1.getSurName() << " annual salary is: " << employee1.getMonthlySalary() * 12 << std::endl;
    employee1.setMonthlySalary(monthSalary1 * 1.1);
    std::cout << employee1.getName() << " " << employee1.getSurName() << " salary increased by 10%. Now it is " << employee1.getMonthlySalary() * 12 << std::endl;
    std::cout << std::endl;
    
    Employee employee2("Garnik", "Hakobyan", 120000);
    int monthSalary = employee2.getMonthlySalary();

    std::cout << employee2.getName() << " " << employee2.getSurName() << " annual salary is: " << employee2.getMonthlySalary() * 12 << std::endl;
    employee2.setMonthlySalary(monthSalary * 1.1);
    std::cout << employee2.getName() << " " << employee2.getSurName() << " salary increased by 10%. Now it is " << employee2.getMonthlySalary() * 12 << std::endl; 

    return 0;
} 
