#include <iostream>
#include "Account.hpp"

int 
main()
{       
    int startingBalance;
    std::cout << "Enter your current balance: ";
    std::cin >> startingBalance;
    Account accountOwner1(startingBalance - 80);
    std::cout << "\tThe bank has withdrawn  80$ from your account. Because you owe the bank." << std::endl;
    std::cout << "\tPress 1 to fund your account.\n"
              << "\tPress 2 to withdraw from the account.\n"
              << "\tPress 3 to check account balance.\n"
              << "\tInput Number: ";

    int selectionNumber;
    std::cin >> selectionNumber;
    
    if (3 < selectionNumber) {
        std::cout << "Error 1: Wrong number for selection.";
        return 1;
    }
    if (0 >= selectionNumber) {
        std::cout << "Error 1: Wrong number for selection.";
        return 1;
    }

    if (1 == selectionNumber) {
        int inputMoney;
        std::cout << "\tEnter the amount to enter: ";
        std::cin >> inputMoney;
        accountOwner1.credit(inputMoney);
        std::cout << "\tYour current balance: " << accountOwner1.getBalance() << std::endl;
    }

    if (2 == selectionNumber) {
        int inputMoney;
        std::cout << "\tEnter the amount to withdraw from the account: ";
        std::cin >> inputMoney;
        accountOwner1.debit(inputMoney);
        std::cout << "\tYour current balance: " << accountOwner1.getBalance() << std::endl;
    }

    if (3 == selectionNumber) {
        std::cout << "\tYour current balance: " << accountOwner1.getBalance() << std::endl;
    }

    std::cout << "\nThis is where the second user starts" << std::endl;
    std::cout << "\tEnter your current balance: ";
    std::cin >> startingBalance;
    Account accountOwner2(startingBalance + 50);   
    std::cout << "\tMentor Graphics Raises Salary by  50$" << std::endl;
    std::cout << "\tPress 1 to fund your account.\n"
              << "\tPress 2 to withdraw from the account.\n"
              << "\tPress 3 to check account balance.\n"
              << "\tInput Number: ";

    std::cin >> selectionNumber;
  
    if (3 < selectionNumber) {
        std::cout << "Error 1: Wrong number for selection.";
        return 1;
    }
    if (0 >= selectionNumber) {
        std::cout << "Error 1: Wrong number for selection.";
        return 1;
    }
  
    if (1 == selectionNumber) {
        int inputMoney;
        std::cout << "\tEnter the amount to enter: ";
        std::cin >> inputMoney;
        accountOwner2.credit(inputMoney);
        std::cout << "\tYour current balance: " << accountOwner2.getBalance() << std::endl;
    }
  
    if (2 == selectionNumber) {
        int inputMoney;
        std::cout << "\tEnter the amount to withdraw from the account: ";
        std::cin >> inputMoney;
        accountOwner2.debit(inputMoney);
        std::cout << "\tYour current balance: " << accountOwner2.getBalance() << std::endl;
    }
  
    if (3 == selectionNumber) {
        std::cout << "\tYour current balance: " << accountOwner2.getBalance() << std::endl;
    }
  
    return 0;
}
