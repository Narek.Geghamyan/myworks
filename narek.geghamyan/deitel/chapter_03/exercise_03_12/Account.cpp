#include <iostream>
#include "Account.hpp"

Account::Account(int startingBalance)
{
    setBalance(startingBalance);   
}

void
Account::credit(int inputMoney)
{
    bankBalance_ += inputMoney;
}

void
Account::debit(int inputMoney)
{    
    if (inputMoney > bankBalance_) {
        std::cout << "Info 1: The amount entered exceeds the current balance." << std::endl;
        return;
   }

    bankBalance_ -= inputMoney;
}

int
Account::getBalance()
{
    return bankBalance_;
}

void
Account::setBalance(int accountBalance)
{
    bankBalance_ = accountBalance;

    if (accountBalance < 0) {
        std::cout << "Info 2: Given balance is negative. Resetting to 0..." << std::endl;
        bankBalance_ = 0;
    }
}

