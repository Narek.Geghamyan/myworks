class Account
{
public:
    Account(int startingBalance);
    void credit(int inputMoney);
    void debit(int inputMoney);
    int getBalance();
    void setBalance(int accountBalance);
private:
     int bankBalance_;
};

