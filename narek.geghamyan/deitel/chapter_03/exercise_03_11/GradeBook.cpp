#include <iostream>
#include "GradeBook.hpp"

GradeBook::GradeBook(std::string nameOfCourse, std::string nameOfTeacher)
{
    setCourseName(nameOfCourse);
    setTeacherName(nameOfTeacher);
}

void 
GradeBook::setCourseName(std::string nameOfCourse)
{
    courseName_ = nameOfCourse;
}

std::string
 GradeBook::getCourseName()
{
    return courseName_;
}

void 
GradeBook::setTeacherName(std::string nameOfTeacher)
{
    teacherName_ = nameOfTeacher;
}

std::string 
GradeBook::getTeacherName()
{
    return teacherName_;
}

void 
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for " << getCourseName() << " ! " << std::endl;
    std::cout << "This course is presented by " << getTeacherName() << std::endl;
}
   
