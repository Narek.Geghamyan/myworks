#include <string>

class GradeBook
{
public:
    GradeBook(std::string nameOfCourse, std::string nameOfTeacher);
    void setCourseName(std::string nameOfCourse);
    std::string getCourseName();
    void setTeacherName(std::string nameOfTeacher);
    std::string getTeacherName();
    void displayMessage();
    
private:
    std::string courseName_;
    std::string teacherName_;
};       
