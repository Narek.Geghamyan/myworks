#include <iostream>

int gcd(const int number1, const int number2);

int
main()
{
    std::cout << "Input two numbers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << "Greatest common divisor: " << gcd(number1, number2) << std::endl;
    return 0;
}

int
gcd(int number1, int number2)
{
    /// Euclidean algorithm.
    while (number1 != 0 && number2 != 0) {
        if (number1 > number2) {
            number1 %= number2;
        } else {
            number2 %= number1; 
        }
        
    }

    const int gcd = number1 + number2;
    return gcd;
}
