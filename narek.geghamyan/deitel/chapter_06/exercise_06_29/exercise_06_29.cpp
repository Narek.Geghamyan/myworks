#include <iostream>

bool perfect(const int number);
void printCounterNumber(const int number);

int
main()
{
    for (int number = 1; number <= 1000; ++number) {
        const bool perfectNumber = perfect(number);
        if (perfectNumber) {
            std::cout << "\tPerfect number: " << number << std::endl;
        }
    }
    return 0;
}


void
printCounterNumber(const int number)
{
    std::cout << "\nDivisors of this perfect number: ";
    for (int counter = 1; counter < number; ++counter) {
        if (0 == number % counter) {
            std::cout << counter << " ";
        }
    }
}

bool
perfect(const int number)
{
    int perfectNumber = 0;
    for (int counter = 1; counter < number; ++counter) {
        if (0 == number % counter) {
            perfectNumber += counter;
        }
    }

    if (number == perfectNumber) {
        printCounterNumber(number);
    }

    return number == perfectNumber;
}


