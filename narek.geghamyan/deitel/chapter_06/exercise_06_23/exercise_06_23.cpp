#include <iostream>
#include <cassert>

void printSquare (const int side, const char fillCharacter);

int
main()
{
    std::cout << "Input side: ";
    int side;
    std::cin >> side;
    if (side <= 0) {
        std::cout << "Error 1: Side cannot be negative or zero.\a" << std::endl;
        return 1;
    }

    std::cout << "Input simbol: ";
    char fillCharacter;
    std::cin >> fillCharacter;
    printSquare(side, fillCharacter);
    return 0;
}

void
printSquare(const int side, const char fillCharacter)
{
    assert(side > 0);
    for (int counter1 = 0; counter1 < side; ++counter1) {
        for (int counter2 = 0; counter2 < side; ++counter2) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

