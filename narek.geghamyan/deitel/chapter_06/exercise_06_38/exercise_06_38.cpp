#include <iostream>
#include <cstring>

int
main()
{
    ::srand(::time(0));
    while (true) {
        std::cout << "My number is between 1 and 1000.\n"
                  << "Can you guess it?\n"
                  << "Write your first guess: ";
        const int number = 1 + ::rand() % 1000;
        while (true) {
            int guess;
            std::cin >> guess;
            if (guess < number) {
                std::cout << "Too few. Try again: ";
                continue;
            }
            if (guess > number) {
                std::cout << "Too much. Try again: ";
                continue;
            }
            std::cout << "Fine! You guessed the number!" << std::endl;
            break;
        }
        std::cout << "Want to play some more (0 - Yes or 1 - No)?: ";
        bool selectionNumber;
        std::cin >> selectionNumber;
        if (selectionNumber) {
            std::cout << "Exiting..." << std::endl;
            break;
        }
    }
    return 0;
}

