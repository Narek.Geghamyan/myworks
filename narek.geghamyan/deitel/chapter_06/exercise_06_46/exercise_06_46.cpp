#include <iostream>

int
main()
{
    static int counter = 1;
    std::cout << counter << std::endl;
    ++counter;
    if (6 == counter) {
        return 0;           /// Without a breakpoint, we cannot recursively call the main function, the compiler will not allow it, saying that we cannot call main from the program.
    }
    main();
    return 0; 
}
