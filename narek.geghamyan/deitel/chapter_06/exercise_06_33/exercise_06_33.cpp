#include <iostream>
#include <cassert>
#include <cmath>

int qualityPoints(const int average);

int
main()
{
    std::cout << "Input student average grade: ";
    int average;
    std::cin >> average;
    if (average < 0 || average > 100) {
        std::cout << "Error 1: Average grade cannot be negative or more than a hundred." << std::endl;
        return 1;
    }
    std::cout << "Student grade: " << qualityPoints(average) << std::endl;
    return 0;
}

int
qualityPoints(int average)
{
    assert(average >= 0 && average <= 100);
    if (100 == average) { /// Condition  necessary for the reason that 100 cannot be in my ostanavnoy formula.
        --average;
    }
    return average / 10 - 5;
}

