#include <iostream>
#include <cassert>
   
unsigned long int factorial(const unsigned int number);
   
int
main()
{
    for (int number = 1; number <= 10; ++number) {
        std::cout << number << "! = " << factorial(number) << std::endl;
    }
    return 0;
}

unsigned long int
factorial(const unsigned int number)
{
    if (number <= 1) {
        std::cout << number << " = ";
        return 1;
    }

    std::cout << number << " * ";
    return number * factorial(number - 1);
}

