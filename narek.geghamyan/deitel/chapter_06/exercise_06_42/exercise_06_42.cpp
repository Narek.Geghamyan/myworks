#include <iostream>
#include <cassert>

void hanoi(const int height, const int pillar1, const int pillar3);

int
main()
{
    std::cout << "Enter the height: ";
    int height;
    std::cin >> height;
    if (height <= 0) {
        std::cerr << "Error 1: Height cannot be negative or zero.\a" << std::endl;
        return 1;
    }
    hanoi(height, 1, 3);
    return 0;
}

void
hanoi(const int height, const int pillar1, const int pillar3)
{
    if (1 == height) {
        std::cout << pillar1 << " -> " << pillar3 << std::endl;
        return;
    }
    const int pillar2 = 6 - pillar1 - pillar3;
    hanoi(height - 1, pillar1, pillar2);
    hanoi(1, 1, 3);
    hanoi(height - 1, pillar2, pillar3); 
}

