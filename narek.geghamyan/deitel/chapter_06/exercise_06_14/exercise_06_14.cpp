#include <iostream>
#include <cmath>

double roundToInteger(const double number);
double roundToTenths(const double number);
double roundToHundredths(const double number);
double roundToThousandths(const double number);

int
main()
{
    std::cout << "Input number: ";
    double number;
    std::cin >> number;
    std::cout << "Original number: " << number << std::endl;
    std::cout << "Rounded to integer: " << roundToInteger(number) << std::endl;
    std::cout << "Rounded to tenths: " << roundToTenths(number) << std::endl;
    std::cout << "Rounded to hundredths: " << roundToHundredths(number) << std::endl;
    std::cout << "Rounded to thousandths: " << roundToThousandths(number) << std::endl;
    return 0;
}

double
roundToInteger(const double number)
{
    return ::floor(number);
}

double
roundToTenths(const double number)
{
    return ::floor(number * 10 + .5) / 10;
}

double
roundToHundredths(const double number)
{
    return ::floor(number * 100 + .5) / 100;
}

double
roundToThousandths(const double number)
{
    return ::floor(number * 1000 + .5) / 1000;
}
