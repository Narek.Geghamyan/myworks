#include <iostream>
#include <cassert>

int time(const int hour, const int minute, const int seconds);

int
main()
{
    int globalHour1 = 0, globalMinute1 = 0, globalSeconds1 = 0;
    int globalHour2 = 0, globalMinute2 = 0, globalSeconds2 = 0;
    for (int counter = 1; counter <= 2; ++counter) {
        std::cout << counter << "nd moment" << std::endl;
        std::cout << "\tInput hour: ";
        int hour;
        std::cin >> hour;
        if (hour > 12 || hour <= 0) {
            std::cerr << "Error 1: We have twelve hour cycle with positive numbers.\a" << std::endl;
            return 1;
        }
        std::cout << "\tInput minute: ";
        int minute;
        std::cin >> minute;
        if (minute > 60 || minute <= 0) {
            std::cerr << "Error 2: Minutes cannot be more than sixty or negative.\a" << std::endl;
            return 2;
        }
        std::cout << "\tInput seconds: ";
        int seconds;
        std::cin >> seconds;
        if (minute > 60 || minute < 0) {
            std::cerr << "Error 3: Seconds cannot be more than sixty or negative.\a" << std::endl;
            return 3;
        }

        switch (counter) {
        case 1: globalHour1 = hour, globalMinute1 = minute, globalSeconds1 = seconds;
        case 2: globalHour2 = hour, globalMinute2 = minute, globalSeconds2 = seconds;
        }
    }

    if ((globalHour1 + globalMinute1 + globalSeconds1) >= globalHour2 + globalMinute2 + globalSeconds2) {
        std::cout << "From the first moment to the second moment passed: " << time(globalHour1 - globalHour2, globalMinute1 - globalMinute2, globalSeconds1 - globalSeconds2) << std::endl;
    } else {
        std::cout << time(globalHour2 - globalHour1, globalMinute2 - globalMinute1, globalSeconds2 - globalSeconds1) << std::endl;
    }
    return 0;
}

int
time(const int hour, const int minute, const int seconds)
{
    assert(hour <= 12 && hour > 0 && minute > 0 && minute <= 60 && seconds > 0 && seconds <= 60);
    const int secondsHour = hour * 3600;
    const int secondsMinute = minute * 60;
    const int hoursInSeconds = secondsHour + secondsMinute + seconds;
    return hoursInSeconds;
}

