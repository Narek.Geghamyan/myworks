#include <iostream>
#include <cmath>
#include <unistd.h>
#include <iomanip>

double distance(const double x1, const double x2, const double y1, const double y2);

int
main()
{
    std::cout << "Enter the coordinates: ";
    double x1;
    std::cin >> x1;
    double y1;
    std::cin >> y1;
    double x2;
    std::cin >> x2;
    double y2;
    std::cin >> y2;
    std::cout << "Distance: " << std::setprecision(5) << distance(x1 ,y1, x2, y2) << std::endl;
    return 0;
}

inline double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double deltaX = x1 - x2;
    const double deltaY = y1 - y2;
    const double value = std::sqrt(deltaX * deltaX + deltaY * deltaY);
    return value;
}

