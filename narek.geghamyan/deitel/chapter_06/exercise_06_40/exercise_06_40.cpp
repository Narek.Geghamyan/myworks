#include <iostream>
#include <cassert>

int power(const int base, const int exponent);

int
main()
{
    std::cout << "Input base: ";
    int base;
    std::cin >> base;
    if (base < 0) {
        std::cerr << "Error 1: Base cannot be negative.\a" << std::endl;
        return 1;
    }

    std::cout << "Input exponent: ";
    int exponent;
    std::cin >> exponent;
    if (exponent < 0) {
        std::cerr << "Error 2: Exponent cannot be negative.\a" << std::endl;
        return 2;
    }
    std::cout << base << " ^ " << exponent << " = " << power(base, exponent) << std::endl;
    return 0;
}

int
power(const int base, const int exponent)
{
    assert(base >= 0 && exponent >= 0);
    if (0 == exponent) {
         return 1;
    }

    if(0 == exponent % 2) {
        return power(base * base, exponent / 2);
    }

    return base * power(base, exponent - 1);
}

