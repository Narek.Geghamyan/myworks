#include <iostream>
#include <cassert>
#include <iomanip>
#include <cmath>

void teacher();
void goodReview();
void badReview();
int getRandomNumber(const int level);
int getOperationResult(int operationNumber, const int number1, const int number2);
char getOperation(int operationNumber);

int
main()
{
    ::srand(::time(0));
    teacher();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
teacher()
{
    int answerCounter = 0;
    int rightAnswer = 0;
    int wrongAnswer = 0;
    while (true) {
        std::cout << "Select difficulty level (1, 2, 3): ";
        int level;
        std::cin >> level;
        if (level > 3 || level < 1) {
            std::cerr << "Error 1: You only have three options to choose from.\a" << std::endl;
            ::exit(1);
        }
        std::cout << "Select arithmetic operation (1 - (+) | 2 - (-) | 3 - (*) | 4 - (/) | 5 - random): ";
        int operationNumber;
        std::cin >> operationNumber;
        const char operation = getOperation(operationNumber);
        const int number1 = getRandomNumber(level);
        const int number2 = getRandomNumber(level);
        while (true) {
            const int correctSolution = getOperationResult(operationNumber, number1, number2);
            std::cout << "How much will be: " << number1 << " " << operation << " " << number2;
            std::cout << "\nAnswer: ";
            int answer;
            std::cin >> answer;
            ++answerCounter;
            if (correctSolution == answer) {
                ++rightAnswer;
                goodReview();
                break;
            }
            ++wrongAnswer;
            badReview();
        }
        std::cout << "Do you want to continue? (0 - Yes, 1 - No): ";
        bool selectionNumber3;
        std::cin >> selectionNumber3;
        if (selectionNumber3) {
            break;
        }
    }

    if (answerCounter * 0.75 > static_cast<double>(rightAnswer)) {
        std::cout << "Please ask your teacher to help you." << std::endl;
    }
}

void
goodReview()
{
    const int reviewText = 1 + ::rand() % 4;
    switch (reviewText) {
    case 1: std::cout << "Very well!"                       << std::endl; break;
    case 2: std::cout << "Good!"                            << std::endl; break;
    case 3: std::cout << "Wonderful job!"                   << std::endl; break;
    case 4: std::cout << "Keep working in the same spirit!" << std::endl; break;
    default: assert(0); break;
    }
}

void
badReview()
{
    const int reviewText = 1 + ::rand() % 4;
    switch (reviewText) {
    case 1: std::cout << "No, try again"           << std::endl; break;
    case 2: std::cout << "Wrong. Please try again" << std::endl; break;
    case 3: std::cout << "Don't give up!"          << std::endl; break;
    case 4: std::cout << "No.Keep trying."         << std::endl; break;
    default: assert(0); break;
    }
}

int
getRandomNumber(const int level)
{
    assert(level <= 3 && level >= 1 && "Assert 1: You only have three options to choose from.\a");
    const int range = ::pow(10, level);
    const int number = ::rand() % range;
    return number;
}

int
getOperationResult(int operationNumber, const int number1, const int number2)
{
    if (5 == operationNumber) {
         operationNumber = 1 + ::rand() % 4;
    }

    int rightSolution = 0;
    switch (operationNumber) {
    case 1: rightSolution = number1 + number2; break;
    case 2: rightSolution = number1 * number2; break;
    case 3: rightSolution = number1 - number2; break;
    case 4: rightSolution = number1 / number2; break;
    default: std::cerr << "Error 2: You only have four options to choose from.\a" << std::endl;
             ::exit(2);
    }
    return rightSolution;
}

char
getOperation(int operationNumber)
{
    char operation = 0;
    if (5 == operationNumber) {
        operationNumber = 1 + rand() % 4;
    }

    switch (operationNumber) {
    case 1: operation = '+'; break;
    case 2: operation = '*'; break;
    case 3: operation = '-'; break;
    case 4: operation = '/'; break;
    default: std::cerr << "Error 3: You only have four options to choose from.\a" << std::endl;
            ::exit(3); 
    }

    return operation;
}
