a) х = fabs(7.5) ----> 7.5
b) x = floor(7.5) ----> 7
c) x = fabs(0.0) ----> 0
d) x = ceil(0.0) ----> 0
e) x = fabs(-6.4) ----> 6.4
f) x = ceil(-6.4) ----> -6
g) x = ceil(-fabs(-8 + floor(-5.5))) ----> -14
