#include <iostream>
#include <cassert>

int integerPower(const int base, unsigned int exponent);

int
main()
{
    std::cout << "Input base: ";
    int base;
    std::cin >> base;
    std::cout << "Input positive exponent: ";
    int exponent;
    std::cin >> exponent;
    if (exponent <= 0) {
        std::cout << "Error 1: Exponent cannot be negative or zero." << std::endl;
        return 1;
    }

    std::cout << base << " ^ " << exponent << " = " << integerPower(base, exponent) << std::endl;
    return 0;
}

int
integerPower(int base, unsigned int exponent) 
{ 
    int result = 1;
    while (exponent > 0) {
        if (exponent & 1) {  /// I studied bit wise operators and used one of them here i hope this is not a violation of the rules.
            result *= base;
        }
        exponent /= 2; 
        base *= base; 
    } 
    return result; 
} 
  
