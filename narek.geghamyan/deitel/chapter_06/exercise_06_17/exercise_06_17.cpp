#include <iostream>

int
main()
{
    ::srand(::time(0));
    /// a)
    std::cout << "Random number (2, 4, 6, 8, 10): " << 2 + (::rand() % 5) * 2 << std::endl;
    /// b)
    std::cout << "Random number (3, 5, 7, 9, 11): " << 3 + (::rand() % 5) * 2 << std::endl;
    /// c)
    std::cout << "Random number (6, 10, 14, 18, 22): " << 6 + (::rand() % 5) * 4 << std::endl;
    return 0;
}
