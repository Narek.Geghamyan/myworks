#include <iostream>

void teacher();

int
main()
{
    ::srand(::time(0));
    teacher();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
teacher()
{
    while (true) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            std::cout << "How much will be: " << number1 << " * " << number2;
            std::cout << "\tAnswer: ";
            int answer;
            std::cin >> answer;
            if (number1 * number2 == answer) {
                std::cout << "Very well!" << std::endl;
                break;
            }
            std::cout << "No, try again" << std::endl;
        }
        std::cout << "Do you want to continue? (0 - Yes, 1 - No): ";
        bool selectionNumber;
        std::cin >> selectionNumber;
        if (selectionNumber) {
            break;
        }
    }
}

