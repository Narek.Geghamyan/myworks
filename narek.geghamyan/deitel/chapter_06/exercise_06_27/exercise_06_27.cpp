#include <iostream>
#include <iomanip>

double celsius(const int temperature);
double fahrenheit(const int temperature);
int
main()
{
    for (int temperature = 0; temperature <= 100; ++temperature) {
        std::cout << std::setw(8) << temperature << " celsius = " << std::setw(6) << fahrenheit(temperature) << " fahrenheit";
        if (0 == temperature % 5) {
            std::cout << std::endl;
        }
    }

    std::cout << std::endl;

    for (int temperature = 32; temperature <= 212; ++temperature) {
        std::cout << std::setw(8) << temperature << " fahrenheit = " << std::setw(4) << celsius(temperature) << " celsius";

        if  (0 == temperature % 5) {
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
    return 0;
}

double
celsius(const int temperature)
{
    const double fahrenheit = static_cast<double>(temperature) - 32 * 5/9; /// Fahrenheit formula.
    return fahrenheit;
}

double
fahrenheit(const int temperature)
{
    const double celsius = static_cast<double>(temperature) * 9/5 + 32; /// celsius formula.
    return celsius;
}

