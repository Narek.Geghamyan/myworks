#include <iostream>

int
main()
{
    ::srand(time(0));
    ///a
    int n = 1 + rand() % 2;
    std::cout << "Random number in the range from 1 to 2: " << n << std::endl;
    ///b
    n = 1 + rand() % 100;
    std::cout << "Random number in the range from 1 to 100: " << n << std::endl;
    ///c
    n = rand() % 10;
    std::cout << "Random number in the range from 0 to 9: " << n << std::endl;
    ///d
    n = 1000 + rand() % 113;
    std::cout << "Random number in the range from 1000 to 1112: " << n << std::endl;
    ///e
    n =  -1 + rand() % 3;
    std::cout << "Random number in the range from -1 to 1: " << n << std::endl;
    ///f
    n = -3 + rand() % 15;
    std::cout << "Random number in the range from -3 to 11: " << n << std::endl;
    return 0;
}
