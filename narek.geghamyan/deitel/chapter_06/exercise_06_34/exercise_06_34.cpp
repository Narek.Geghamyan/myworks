#include <iostream>

int flip();

int
main()
{
    ::srand(::time(0));
    int totalEagle = 0;
    int totalTails = 0;
    for (int throwing = 1; throwing <= 100; ++throwing) {
        const int coinSide = flip();
        if (0 == coinSide) {
            std::cout << "eagle" << std::endl;
            ++totalEagle;
        } else {
            std::cout << "tails" << std::endl;
            ++totalTails;
        }
    }

    std::cout << "Total eagle: " << totalEagle << std::endl;
    std::cout << "Total tails: " << totalTails << std::endl;
    return 0;
}

int
flip()
{
    const int coinSide = ::rand() % 2;
    return coinSide;
}
