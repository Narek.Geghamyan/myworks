#include <iostream>
#include <cassert>

void teacher();
void goodReview();
void badReview();

int
main()
{
    ::srand(::time(0));
    teacher();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
teacher()
{
    while (true) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            std::cout << "How much will be: " << number1 << " * " << number2;
            std::cout << "\nAnswer: ";
            int answer;
            std::cin >> answer;
            if (number1 * number2 == answer) {
                goodReview();
                break;
            }
            badReview();
        }
        std::cout << "Do you want to continue? (0 - Yes, 1 - No): ";
        bool selectionNumber3;
        std::cin >> selectionNumber3;
        if (selectionNumber3) {
            break;
        }
    }
}

void
goodReview()
{
    const int reviewText = 1 + ::rand() % 4;
    switch (reviewText) {
    case 1: std::cout << "Very well!"                       << std::endl; break;
    case 2: std::cout << "Good!"                            << std::endl; break;
    case 3: std::cout << "Wonderful job!"                   << std::endl; break;
    case 4: std::cout << "Keep working in the same spirit!" << std::endl; break;
    default: assert(0); break;
    }
}

void
badReview()
{
    const int reviewText = 1 + ::rand() % 4;
    switch (reviewText) {
    case 1: std::cout << "No, try again"           << std::endl; break;
    case 2: std::cout << "Wrong. Please try again" << std::endl; break;
    case 3: std::cout << "Don't give up!"          << std::endl; break;
    case 4: std::cout << "No.Keep trying."         << std::endl; break;
    default: assert(0); break;
    }
}

