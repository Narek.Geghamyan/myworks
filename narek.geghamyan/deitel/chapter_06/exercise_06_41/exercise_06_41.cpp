#include <iostream>
#include <limits>
#include <cfloat>
#include <cassert>
#include <iomanip>
#include <cmath>

template <typename T>
void fibonacciMax(int& counter, T& currentNumber, T& nextNumber);

int
main()
{
    int counterInt = 1;
    int currentNumberInt = 0;
    int nextNumberInt = 1;
    fibonacciMax(counterInt, currentNumberInt, nextNumberInt);
    std::cout << "The largest number fibonacci on this system (int) (counter = " << counterInt << "): " << nextNumberInt << std::endl;
    int counterDouble = counterInt;
    double currentNumberDouble = static_cast<double>(currentNumberInt);
    double nextNumberDouble = static_cast<double>(nextNumberInt);
    fibonacciMax(counterDouble, currentNumberDouble, nextNumberDouble);
    std::cout << "The largest number fibonacci on this system (double) (counter = " << counterDouble  << "): " << std::fixed << std::setprecision(0) << nextNumberDouble << std::endl;
    return 0;
}

template <typename T>
void
fibonacciMax(int& counter, T& currentNumber, T& nextNumber)
{
    while (true) {
        const T fibonacciNumber = currentNumber + nextNumber;
        if (fibonacciNumber < 0 || fibonacciNumber > std::numeric_limits<T>::max()) {
            break;
        }
        currentNumber = nextNumber;
        nextNumber = fibonacciNumber;
        ++counter;
    }
}

