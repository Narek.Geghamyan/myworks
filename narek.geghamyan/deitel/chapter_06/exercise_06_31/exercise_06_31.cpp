#include <iostream>
#include <cassert>

int reversingOrderOfNumbers(int number);
int division(const int number1, const int number2);
int remainder(const int number1, const int number2);

int
main()
{
    std::cout << "Input number (1 - 100000): ";
    int number;
    std::cin >> number;
    if (number < 1 || number > 100000) {
        std::cerr << "Error 1: Entered number out of range.\a" << std::endl;
        return 1;
    }
    std::cout << "Reverse number: " << reversingOrderOfNumbers(number) << std::endl;
    return 0;
}

int
division(const int number1, const int number2)
{
    assert(number2 != 0);
    int dividedNumber = number1 / number2;
    return dividedNumber;
}

int
remainder(const int number1, const int number2)
{
    assert(number2 != 0);
    int remainderDigit = number1 % number2;
    return remainderDigit;
}

int
reversingOrderOfNumbers(int number)
{
    assert(number >= 1 && number <= 100000);
    int reversedNumber = 0;
    int divisor = 100000;
    while (number < divisor) {
        divisor = division(divisor, 10);
    }

    while (number != 0) {
        reversedNumber += remainder(number, 10) * divisor;
        number = division(number, 10);
        divisor = division(divisor, 10);
    }

    return reversedNumber;
}

