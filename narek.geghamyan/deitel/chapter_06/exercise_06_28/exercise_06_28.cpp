#include <iostream>

double searchMaximum(const double number1, const double number2, const double number3);

int
main()
{
    std::cout << "Input three numbers (double): ";
    double number1, number2, number3;
    std::cin >> number1 >> number2 >> number3;
    std::cout << "Maximum: " << searchMaximum(number1, number2, number3) << std::endl;
    return 0;
}

double
searchMaximum(const double number1, const double number2, const double number3)
{
    double maximum = number1;
    if (number2 > maximum) {
        maximum = number2;
    }

    if (number3 > maximum) {
        maximum = number3;
    }

    return maximum;
}
