#include <iostream>
#include <cassert>

int whole(const int number1, const int number2);
int remainder(const int number1, const int number2);
void divisionIntoParts(const int randomNumber); 

int
main()
{
    std::cout << "Input number (1 - 32767): ";
    int randomNumber;
    std::cin >> randomNumber;
    if (randomNumber > 32767 || randomNumber < 1) {
        std::cout << "Error 1: Entered number out of range.\a" << std::endl;
        return 1;
    }
    
    std::cout << "Original number: " << randomNumber << std::endl;
    std::cout << "Divided into parts: ";
    divisionIntoParts(randomNumber);
    return 0;
}

int
whole(const int number1, const int number2)
{
    assert(number2 != 0);
    const int wholeDigit = number1 / number2;
    return wholeDigit;
}

int
remainder(const int number1, const int number2)
{
    assert(number2 != 0);
    const int remainderNumber = number1 % number2;
    return remainderNumber;
}

void
divisionIntoParts(const int randomNumber)
{
    assert(randomNumber >= 1 && randomNumber <= 32767);
    const static int DIVIDER = 10;
    int numberSize = 10000;
    while (numberSize > randomNumber) {
        numberSize = whole(numberSize, DIVIDER);
    }
    while (numberSize != 0) {
        const double separateDigit = remainder(whole(randomNumber, numberSize), DIVIDER);
        if (separateDigit < DIVIDER && separateDigit >= 0) {
            std::cout << randomNumber / numberSize % DIVIDER << " ";
        } 
        numberSize /= DIVIDER;
    }
    std::cout << std::endl;
}

