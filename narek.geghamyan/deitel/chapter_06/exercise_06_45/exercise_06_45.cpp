#include <iostream>

int gcd(const int number1, const int number2);

int
main()
{
    std::cout << "Input two numbers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << "Greatest common divisor: " << gcd(number1, number2) << std::endl;
    return 0;
}

int
gcd(const int number1, const int number2)
{
    if (0 == number2) {
        return number1;
    }

    return gcd(number2, number1 % number2);
}

