#include <iostream>
#include <cassert>

inline double circleArea(const double radius);

int
main()
{
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;
    if (radius < 0) {
        std::cerr << "Error 1: Radius cannot be negative.\a" << std::endl;
        return 1;
    }
    std::cout << "Area of a circle: " << circleArea(radius) << std::endl;
    return 0;
}

inline double
circleArea(const double radius)
{   
    assert(radius >= 0 && "Assert 1: Radius cannot be negative.\a"); 
    const double PI = 3.14;
    const double area = PI * (radius * radius);
    return area;
}
