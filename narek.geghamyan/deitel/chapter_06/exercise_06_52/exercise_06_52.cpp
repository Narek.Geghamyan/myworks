#include <iostream>
#include <cmath>

int
main()
{
    std::cout << "Input number: ";
    double number1;
    std::cin >> number1;
    std::cout << "Original number: " << number1 << std::endl;
    std::cout << "Rounding " << number1 << " to the smallest integer,not less " << number1 << ": " << ::ceil(number1) << std::endl;
    std::cout << "Trigonometric cosine " << number1 << " (" << number1 << " in radians): " << ::cos(number1) << std::endl;
    std::cout << "Exponential function ex: " << ::exp(number1) << std::endl;
    std::cout << "Absolute value: " << ::fabs(number1) << std::endl;
    std::cout << "Rounding " << number1 << " to the largest integer,not exceeding " << number1 << ": " << ::floor(number1) << std::endl;
    std::cout << "Input number: ";
    double number2;
    std::cin >> number2;
    std::cout << "Remainder of " << number1 << " / " << number2 << " as number float point: " << ::fmod(number1, number2) <<std::endl;
    std::cout << "Natural logarithm: " << ::log(number1) << std::endl;
    std::cout << "Decimal logarithm: " << ::log10(number1) << std::endl;
    std::cout << "Input exponent: ";
    int exponent;
    std::cin >> exponent;
    std::cout << number1 << " to the power of " << exponent << ": " << ::pow(number1, exponent) << std::endl;
    std::cout << "Trigonometric sine: " << ::sin(number1) << std::endl;
    std::cout << "square root of " << number1 << " (where " << number1 << " is a non-negative value: " << ::sqrt(number1) << std::endl;
    std::cout << "Trigonometric tangent: " << tan(number1) << std::endl;
    return 0;
}
