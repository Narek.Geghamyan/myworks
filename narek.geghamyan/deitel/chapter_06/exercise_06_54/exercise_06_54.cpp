#include <iostream>
#include <cstdlib>
#include <cassert>

int rollDice();
bool game();
void printBankBalance(const unsigned int bankBalance);
void review();

int
main ()
{
    ::srand(::time(0));
    int bankBalance = 1000;
    while (bankBalance > 0) {
        printBankBalance(bankBalance);
        std::cout << "Enter your bet: ";
        int wager = 0;
        while (true) {
            std::cin >> wager;
            if (wager <= bankBalance) {
                break;
            }
            std::cout << "There are not enough funds in your account, try again." << std::endl;  
        }
        const bool gameResult = game();
        if (gameResult) {
            bankBalance += wager;
        } else {
            bankBalance -= wager;
        }

        if (0 == bankBalance) {
            std::cout << "Sorry but you went bankrupt.\a" << std::endl;
            printBankBalance(bankBalance);
        }
    }
    return 0;
}

void
printBankBalance(const unsigned int bankBalance)
{
    std::cout << "Your current bank balance: " << bankBalance << "$" << std::endl;
}

int
dices()
{
    const int dice1 = 1 + ::rand() % 6;
    const int dice2 = 1 + ::rand() % 6;
    const int sum = dice1 + dice2;
    std::cout << "Player rolled " << dice1 << " + " << dice2 << " = " << sum << std::endl;
    return sum;
}

bool
game()
{
    enum Status {CONTINUE, WON, LOST};
    Status gameStatus;
    int myPoint = 0;
    const int sumOfDice = dices();
    switch (sumOfDice) {
    case 7:
    case 11: gameStatus = WON;  break;
    case 2:
    case 3:
    case 12: gameStatus = LOST; break;
    default:
        myPoint = sumOfDice;
        gameStatus = CONTINUE;
        std::cout << "Point is " << myPoint << std::endl;
        break;
    }
    review();
    while (CONTINUE == gameStatus) {
        const int sumOfDice = dices();
        if (sumOfDice == myPoint) {
            gameStatus = WON;
        } else if (7 == sumOfDice) {
            gameStatus = LOST;
        }
        if (WON == gameStatus) {
            std::cout << "Player wins" << std::endl;
            return true;
        }

        std::cout << "Player loses" << std::endl;
        return false;
    }
    return false;
}


void
review()
{
    const int dialogue = 1 + rand() % 3;
    switch (dialogue) {
    case 1: std::cout << "Oh, you're going for broke" << std::endl; break;
    case 2: std::cout << "Keep rolling!"              << std::endl; break;
    case 3: std::cout << "How do you play so cool?"   << std::endl; break;
    default: assert(0);                                             break;
    }
}

