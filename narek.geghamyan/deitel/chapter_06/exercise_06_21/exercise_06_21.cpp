#include <iostream>
#include <iomanip>

bool even(const int number);

int
main()
{
    ::srand(::time(0));
    for (int counter = 1; counter <= 10; ++counter) {
        const int number = 1 + ::rand() % 100;
        const bool functionResult = even(number);
        if (functionResult) {
             std::cout << number << std::setw(2) << " is even" << std::endl;
        } else {
             std::cout << number << std::setw(2) << " is odd" << std::endl;
        }
    }
    return 0;
}

bool
even(const int number)
{
    if (number % 2 == 0) {
        return true;
    }
    return false;
}

