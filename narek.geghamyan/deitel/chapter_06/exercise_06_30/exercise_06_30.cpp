#include <iostream>
#include <cmath>

bool searchPrimeNumber(const int number);

int
main()
{
    for (int number = 2; number <= 10000; ++number) {
        if (searchPrimeNumber(number)) {
            std::cout << "Prime: " << number << std::endl;
        }
    }
    
    return 0;
}

bool
searchPrimeNumber(const int number)
{
    /// spending more than the square root of a number is pointless.
    /// I checked in other ways. This is the most optimal.
    for (int counter = 2; counter <= std::sqrt(number); ++counter) {
        if (0 == number % counter) {
            return false;
        }
    }

    return true;
}

