#include <iostream>

void tripleCallByValue(int count);
void tripleByReference(int& count);

int
main()
{
    std::cout << "Input number: ";
    int count;
    std::cin >> count;
    tripleCallByValue(count);
    std::cout << "Digit changed by passing to function by value: " << count << std::endl;
    
    tripleByReference(count);
    std::cout << "Digit changed by going to function by reference: " << count << std::endl;

    return 0;
}

void
tripleCallByValue(int count)
{
    count *= 3;
}

void
tripleByReference(int& count)
{
    count *= 3;
}

