#include <iostream>
#include <cmath>

int mystery(int number1, int number2);

int
main()
{
    std::cout << "Input two numbers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << "Result: " << mystery(number1, number2) << std::endl;
    return 0;
}

int
mystery(int number1, int number2)
{
    if (number2 < 0) {
        number1 *= -1;
        number2 *= -1;
    }

    if (1 == number2) {
        return number1;
    }

    return number1 + mystery(number1, number2 - 1);
}


