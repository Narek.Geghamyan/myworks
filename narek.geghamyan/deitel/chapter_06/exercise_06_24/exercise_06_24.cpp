#include <iostream>
#include <cassert>

void drawFigure(const int selectionNumber);
void drawTriangle();
void drawSquare();
void drawRhombus();

int
main()
{
    std::cout << "What form do you want to print (1, 2, 3): ";
    int selectionNumber;
    std::cin >> selectionNumber;
    if (selectionNumber > 3 || selectionNumber < 1) {
        std::cout << "Error 1: You must select a number from the list.\a";
        return 1;
    }

    drawFigure(selectionNumber);
    return 0;
}

void
drawFigure(const int selectionNumber)
{
    assert(selectionNumber >= 1 && selectionNumber <= 3);
    switch (selectionNumber) {
    case 1: drawSquare();   break;
    case 2: drawTriangle(); break;
    case 3: drawRhombus();  break;
    default:
        std::cout << "Assert 1: You must select a number from the list.\a" << std::endl;
        assert(0);
    }
}

void
drawSquare()
{
    std::cout << "Input side of square: ";
    int sideOfSquare;
    std::cin >> sideOfSquare;
    if (sideOfSquare < 0) {
        std::cout << "Error 2: The side of square cannot be negative.\a" << std::endl;
        ::exit(2);
    }
    std::cout << "Input symbol: ";
    char symbolOfSquare;
    std::cin >> symbolOfSquare;
    for (int counter1 = 0; counter1 < sideOfSquare; ++counter1) {
        for (int counter2 = 0; counter2 < sideOfSquare; ++counter2) {
            std::cout << symbolOfSquare;
        }
        std::cout << std::endl;
    }
}

void
drawTriangle()
{
    std::cout << "Input side of triangle: ";
    int sideOfTriangle;
    std::cin >> sideOfTriangle;
    if (sideOfTriangle < 0) {
        std::cout << "Error 3: The side of triangle cannot be negative.\a" << std::endl;
        ::exit(3);
    }
    std::cout << "Input symbol of triangle: ";
    char symbolOfTriangle;
    std::cin >> symbolOfTriangle;
    for (int counter1 = 0; counter1 < sideOfTriangle; ++counter1) {
        for (int counter2 = 0; counter2 <= counter1; ++counter2) {
            std::cout << symbolOfTriangle;
        }
        std::cout << std::endl;
    }
}

void
drawRhombus()
{
    std::cout << "Input growth of rhombus: ";
    int growthOfRhombus;
    std::cin >> growthOfRhombus;
    if (growthOfRhombus < 0) {
        std::cout << "Error 4: The growth of rhombus cannot be negative." << std::endl;
        ::exit(4);
    }
    std::cout << "Input symbol of rhombus: ";
    char symbolOfRhombus;
    std::cin >> symbolOfRhombus;
    int center = growthOfRhombus / 2;
    for (int counter1 = -center; counter1 <= center; ++counter1) {
        for (int counter2 = -center; counter2 <= center; ++counter2) {
            if (std::abs(counter1) + std::abs(counter2) > center) {
                std::cout << " ";
            } else {
                std::cout << symbolOfRhombus;
            }
        }
        std::cout << std::endl;
    }
}

