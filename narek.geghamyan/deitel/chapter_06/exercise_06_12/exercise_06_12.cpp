#include <iostream>
#include <cassert>
#include <iomanip>
#include <cmath>

double calculateCharges(const double parkingHours);

int
main()
{
    double total = 0;
    double totalCharge = 0;
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Car\t" << "Hours\t" << "Charge" << std::endl;
    for (int counter = 1; counter <= 3; ++counter) {
        double parkingHours;
        std::cin >> parkingHours;
        if (parkingHours < 0) {
            std::cerr << "Error 1: Hours of parking cannot be negative.\a" << std::endl;
            return 1;
        }

        const double charge = calculateCharges(parkingHours);
        total += parkingHours;
        totalCharge += charge;
        std::cout << counter << "\t" << parkingHours << "\t" << charge << std::endl;
    }

    std::cout << "Total\t" << total << "\t" << totalCharge << std::endl;
    return 0;
}

double
calculateCharges(const double parkingHours)
{
    assert(parkingHours >= 0);

    if (parkingHours >= 19.00) {
        return 10.00;
    }

    if (parkingHours > 3.00) {
        return (::ceil(parkingHours - 3.00)) * 0.50 + 2.00;
    }

    return 2.00;
}

