#include <iostream>
#include <cassert>
#include <iomanip>

bool multiple(const int number1, const int number2);

int
main()
{
    ::srand(::time(0));
    const int number1 = 1 + ::rand() % 100;
    if (0 == number1) {
        std::cerr << "Error 1: The number by which we are dividing cannot be zero.\a" << std::endl;
        return 1;
    }

    for (int counter = 1; counter <= 10; ++counter) {
        const int number2 = 1 + ::rand() % 100;
        const bool multipleResult = multiple(number1, number2);
        if (multipleResult) {
            std::cout << std::setw(2) << number2 << " is multiple of " << number1 << std::endl;
        } else {
            std::cout << std::setw(2) << number2 << " is not multiple of " << number1 << std::endl;
        }
    }
    return 0;
}

bool 
multiple(const int number1, const int number2)
{
    assert(number1 != 0);
    if (number2 % number1 == 0) {
        return true;
    }

    return false;
}

