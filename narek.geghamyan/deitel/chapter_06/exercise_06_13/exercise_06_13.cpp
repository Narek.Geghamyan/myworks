#include <iostream>
#include <cmath>
#include <iomanip>

int
main()
{
    for (int counter = 1; counter <= 5; ++counter) {
        std::cout << "Input number: ";
        double number;
        std::cin >> number;
        const double rounded = ::floor(number + .5);
        std::cout << "Original number: " << number << "\nRounded number: " << rounded << std::endl;
    }
    return 0;
}

