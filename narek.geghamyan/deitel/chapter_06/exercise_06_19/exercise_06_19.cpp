#include <iostream>
#include <cmath>
#include <iomanip>
#include <cassert>

double hypotenuse(const double side1, const double side2);

int
main()
{
    std::cout << "Triangle\t" << "Side 1\t" << "Side 2\t" << "Hypotenuse"<<std::endl;
    std::cout << 1 << "\t\t" << std::fixed << std::setprecision(2) << 3.0 << "\t" << 4.0   << "\t" << hypotenuse(3.0, 4.0)  << std::endl;
    std::cout << 2 << "\t\t" << std::fixed << std::setprecision(2) << 5.0 << "\t" << 12.0  << "\t" << hypotenuse(5.0, 12.0) << std::endl;
    std::cout << 3 << "\t\t" << std::fixed << std::setprecision(2) << 8.0 << "\t" << 15.0  << "\t" << hypotenuse(8.0, 15.0) << std::endl;
    return 0;
}

double
hypotenuse(const double side1, const double side2)
{
    assert(side1 > 0 && side2 > 0);
    const double hypotenuse = std::sqrt(side1 * side1 + side2 * side2);
    return hypotenuse;
}

