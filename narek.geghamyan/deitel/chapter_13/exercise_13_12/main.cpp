#include <iostream>
#include <iomanip>
#include <vector>
#include "headers/Employee.hpp"
#include "headers/SalariedEmployee.hpp"
#include "headers/HourlyEmployee.hpp" 
#include "headers/CommissionEmployee.hpp"
#include "headers/BasePlusCommissionEmployee.hpp"

void
virtualViaRePointer(Employee* const baseClassPtr, const Date& calendar)
{
    if (baseClassPtr->getBirthDate().getMonth() == calendar.getMonth()) { baseClassPtr->plusSalary(100); }
    std::cout << "Birth Date: ";
    baseClassPtr->printBirthDate();
    calendar.printDate();
    baseClassPtr->print();
    std::cout << "Earned $" << baseClassPtr->earnings() << "\n\n";
}

int
main()
{
    std::cout << std::fixed << std::setprecision(2);
    std::vector < Employee* > employees(4);
    employees[0] = new SalariedEmployee("John", "Smith", "111-11-1111", "21/02/2000", 800);
    employees[1] = new HourlyEmployee("Karen", "Price", "222-22-2222", "14/06/1998", 16.75, 40);
    employees[2] = new CommissionEmployee("Sue", "Jones", "333-33-3333", "18/02/1999", 10000, .06);
    employees[3] = new BasePlusCommissionEmployee("Bob", "Lewis", "444-44-4444", "25/11/2001", 5000, .04, 300);

    for (Date calendar("21/05/2000"); calendar.getYear() == 2000; calendar.monthIncrement()) {
        for (size_t i = 0; i < employees.size(); i++) {
            virtualViaRePointer(employees[i], calendar);
        }
    }
    return 0;
}

