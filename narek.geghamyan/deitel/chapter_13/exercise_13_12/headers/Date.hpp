#ifndef __DATE_HPP__
#define __DATE_HPP__

#include <iostream>
#include <string>

class Date
{
    friend std::ostream& operator<<(std::ostream& out, const Date& d);
public:
    Date(const int m = 1, const int d = 1, const int y = 1900);
    Date(const std::string date);
    void setDate(const int d, const int m, const int y);
    Date & operator++();
    Date operator++(int);
    const Date& operator+=(const int additionalDays);
    bool leapYear(const int testYear) const;
    bool endOfMonth(const int testDay) const;
    int getDay() const;
    int getMonth() const;
    int getYear() const;
    void printDate() const;
    void monthIncrement();
private:
    size_t month_;
    size_t day_;
    size_t year_;
    static const int days_[];
    void helpIncrement();
};

#endif /// __DATE_HPP__

