#ifndef __EMPLOYEE_HPP__
#define __EMPLOYEE_HPP__

#include "Date.hpp"
#include <string>

class Employee
{
public:
    Employee(const std::string& firstName, const std::string& lastName, 
             const std::string& socialSecurityNumber, const std::string& birthDate);
    
    void setFirstName(const std::string& firstName);
    const std::string& getFirstName() const;
    void setLastName(const std::string& lastName);
    const std::string& getLastName() const;
    const Date& getBirthDate() const;
    void setSocialSecurityNumber(const std::string& socialSecutityNumber);
    const std::string& getSocialSecurityNumber() const;
    virtual double earnings() const = 0;
    virtual void print() const = 0;
    virtual void plusSalary(const size_t prize) = 0;
    virtual void printBirthDate() const = 0;
private:
    std::string firstName_;
    std::string lastName_;
    std::string socialSecurityNumber_;
    Date birthDate_;
};

#endif // __EMPLOYEE_HPP__

