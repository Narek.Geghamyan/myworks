#ifndef __BASEPLUS_HPP__
#define __BASEPLUS_HPP__

#include "headers/CommissionEmployee.hpp"

class BasePlusCommissionEmployee : public CommissionEmployee
{
public:
    BasePlusCommissionEmployee(const std::string& firstName, const std::string& lastName, const std::string& ssn, const std::string& birthDate,
                               const double sales = 0.0, const double rate = 0.0, const double salary = 0.0);
    
    void setBaseSalary(const double salary);
    double getBaseSalary() const;
    void plusSalary(const size_t prize);
    virtual double earnings() const;
    virtual void print() const;
    virtual void printBirthDate() const;
private:
    double baseSalary_;
};

#endif // __BASEPLUS_HPP__

