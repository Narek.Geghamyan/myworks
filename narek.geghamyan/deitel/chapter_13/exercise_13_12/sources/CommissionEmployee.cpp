#include <iostream>
#include "headers/CommissionEmployee.hpp"

CommissionEmployee::CommissionEmployee(const std::string& first, const std::string& last, const std::string& ssn, 
                                       const std::string& birthDate, const double sales, const double rate)
    : Employee(first, last, ssn, birthDate)
{
    setGrossSales(sales);
    setCommissionRate(rate);
}

void
CommissionEmployee::setCommissionRate(const double rate)
{
    commissionRate_ = ((rate > 0.0 && rate < 1.0) ? rate : 0.0);
}

double
CommissionEmployee::getCommissionRate() const
{
    return commissionRate_;
}

void
CommissionEmployee::setGrossSales(const double sales)
{
    grossSales_ = ((sales < 0.0) ? 0.0 : sales);
}

double
CommissionEmployee::getGrossSales() const
{
    return grossSales_;
}

double
CommissionEmployee::earnings() const
{
    return getCommissionRate() * getGrossSales();
}

void
CommissionEmployee::plusSalary(const size_t prize)
{
    grossSales_ += prize;
}

void
CommissionEmployee::print() const
{
    std::cout << "Commission employee: ";
    Employee::print();
    std::cout << "Gross sales: " << getGrossSales() << "; commission rate: " << getCommissionRate() << std::endl;
}

void
CommissionEmployee::printBirthDate() const
{
    std::cout << getBirthDate().getDay() << '/' << getBirthDate().getMonth() << '/' << getBirthDate().getYear() << std::endl;
}

