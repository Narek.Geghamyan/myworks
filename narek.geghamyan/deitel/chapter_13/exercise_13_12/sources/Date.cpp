#include <iostream>
#include <string>
#include <stdlib.h>
#include "headers/Date.hpp"
#include "headers/Employee.hpp"

const int Date::days_[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
Date::Date(int m, int d, int y)
{
    setDate(m, d, y);
}

Date::Date(const std::string date)
{ 
    std::string d = "";
    d = date[0];
    d += date[1]; 
    day_ = ::atoi(d.c_str());
    std::string m = "";
    m = date[3];
    m += date[4];
    month_ = ::atoi(m.c_str());
    std::string y = "";
    y = date[6];
    y += date[7];
    y += date[8];
    y += date[9];
    year_ = ::atoi(y.c_str());
}

void
Date::monthIncrement()
{
    ++month_;
    if (month_ > 12) {
        ++year_;
        month_ = 1;
    }
}

void
Date::setDate( const int mm, const int dd, const int yy)
{
    month_ = (mm >= 1 && mm <= 12) ? mm : 1;
    year_ = (yy >= 1900 && yy <= 2100) ? yy : 1900;
    
    if (month_ == 2 && leapYear(year_)) {
        day_ = (dd >= 1 && dd <= 29) ? dd : 1;
    } else {
        day_ = (dd >= 1 && dd <= days_[month_]) ? dd : 1;
    }
}
 
Date&
Date::operator++()
{
    helpIncrement();
    return *this;
}

Date
Date::operator++(int)
{
    Date temp = *this;
    helpIncrement();
    return temp; 
}

const Date&
Date::operator+=(const int additionalDays)
{
    for (int i = 0; i < additionalDays; ++i) {
        helpIncrement();
    }
     return *this;
}

bool
Date::leapYear(const int testYear) const
{
    if (testYear % 400 == 0 || (testYear % 100 != 0 && testYear % 4 == 0)) {
        return true;
    } else {
        return false;
    }
}

bool
Date::endOfMonth(const int testDay) const
{
    if (month_ == 2 && leapYear(year_)) {
        return testDay == 29;
    } else {
        return testDay == days_[month_];
    }
}

void
Date::helpIncrement()
{
    if (!endOfMonth(day_)) {
        day_++;
    }
    else {
        if (month_ < 12) {
            month_++;
            day_ = 1;
        }
        else {
            year_++;
            month_ = 1;
            day_ = 1;
        }
    }
}

int
Date::getDay() const
{
    return day_;
}

int
Date::getMonth() const
{
    return month_;
}

int
Date::getYear() const
{
    return year_;
}

std::ostream&
operator<<(std::ostream& output, const Date& d)
{
    const static char* monthName[13] = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
    output << monthName[d.month_] << ' ' << d.day_ << ", " << d.year_;
    
    return output; 
}

void
Date::printDate() const
{
    std::cout << day_ << "/" << month_ << "/" << year_ << std::endl;
}
 
