#ifndef __TRIANGLE_HPP__
#define __TRIANGLE_HPP__

#include "headers/TwoDimensionalShape.hpp"

class Triangle : public TwoDimensionalShape
{
public:
    Triangle(const double side = 1.0);
    virtual double getArea() const;
    virtual void printInfo() const;
    virtual double getSide() const;
private:
    double side_;
};

#endif /// __TRIANGLE_HPP__
