#ifndef __SHAPE_HPP__
#define __SHAPE_HPP__

class Shape
{
public:
    virtual double getArea() const = 0;
    virtual void printInfo() const = 0;
};

#endif ///__SHAPE_HPP__

