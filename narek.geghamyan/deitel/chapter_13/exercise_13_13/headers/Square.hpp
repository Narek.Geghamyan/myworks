#ifndef __SQUARE_HPP__
#define __SQUARE_HPP__

#include "headers/TwoDimensionalShape.hpp"

class Square : public TwoDimensionalShape
{
public:
    Square(const double side = 1.0);
    virtual void printInfo() const;
    virtual double getArea() const;
    virtual double getSide() const;
private:
    double side_;
};

#endif /// __SQUARE_HPP__

