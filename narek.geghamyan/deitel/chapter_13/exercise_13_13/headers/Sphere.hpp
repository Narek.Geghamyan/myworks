#ifndef __SPHERE_HPP__
#define __SPHERE_HPP__

#include "headers/TreeDimensionalShape.hpp"

class Sphere : public TreeDimensionalShape
{
    static const double PI = 3.14159;
public:
    Sphere(const double radius = 1.0);
    virtual void printInfo() const;
    virtual double getArea() const;
    virtual double getVolume() const;
    virtual double getRadius() const;
private:
    double radius_;
};

#endif ///  __SPHERE_HPP__
