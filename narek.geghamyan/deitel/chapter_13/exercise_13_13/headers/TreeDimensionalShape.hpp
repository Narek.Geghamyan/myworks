#ifndef __TREE_DIMENSIONAL_SHAPE_HPP__
#define __TREE_DIMENSIONAL_SHAPE_HPP__

#include "headers/Shape.hpp"

class TreeDimensionalShape : public Shape
{
public:
    virtual double getVolume() const = 0;
    virtual double getArea() const = 0;
};

#endif /// __TREE_DIMENSIONAL_SHAPE_HPP__

