#ifndef __CUBE_HPP__
#define __CUBE_HPP__

#include "headers/TreeDimensionalShape.hpp"

class Cube : public TreeDimensionalShape
{
public:
    Cube(const double cubeSide = 1.0);
    virtual void printInfo() const;
    virtual double getArea() const;
    virtual double getVolume() const;
    virtual double getSide() const;
private:
    double side_;
};

#endif ///  __CUBE_HPP__

