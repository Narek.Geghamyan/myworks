#include <iostream>
#include <cmath>
#include <cassert>
#include "headers/Triangle.hpp"

Triangle::Triangle(const double side)
    : side_(side)
{
    assert(side > 0);
}

void
Triangle::printInfo() const
{
    std::cout << "Triangle: Side = " << getSide() << "\n"
              << "          Area = " << getArea() << "\n\n";
}

double
Triangle::getArea() const
{
    return (::sqrt(3) / 4) * (getSide() * getSide());
}

double
Triangle::getSide() const
{
    return side_;
}


