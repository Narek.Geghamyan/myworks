#include <iostream>
#include <cmath>
#include <cassert>
#include "headers/Cube.hpp"

Cube::Cube(const double cubeSide)
    : side_(cubeSide)
{
    assert(cubeSide > 0);
}

double
Cube::getSide() const
{
    return side_;
}

double
Cube::getArea() const
{
    return 6 * getSide() * getSide();
}

void
Cube::printInfo() const
{
    std::cout << "Cube: Side = " << getSide() << "\n"
              << "      Area = " << getArea() << "\n"
              << "    Volume = " << getVolume() << "\n\n";
}

double
Cube::getVolume() const
{
    return ::pow(getSide(), 3);
}

