#include <iostream>
#include <cassert>
#include "headers/Square.hpp"

Square::Square(const double side)
    : side_(side)
{
    assert(side > 0);
}

void
Square::printInfo() const
{
    std::cout << "Square: Side = " << getSide() << "\n"
              << "        Area = " << getArea() << "\n\n";
}

double
Square::getArea() const
{
    return getSide() * getSide();
}

double
Square::getSide() const
{
    return side_;
}

