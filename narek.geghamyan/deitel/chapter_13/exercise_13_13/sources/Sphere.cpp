#include <iostream>
#include <cmath>
#include <cassert>
#include "headers/Sphere.hpp"

Sphere::Sphere(const double radius)
    : radius_(radius)
{
    assert(radius > 0);
}

double
Sphere::getArea()  const
{
    return 4 * PI * getRadius() * getRadius();
}

double
Sphere::getVolume() const
{
    return (4 / 3) * PI * ::pow(getRadius(), 3);
}

double
Sphere::getRadius() const
{
    return radius_;
}

void
Sphere::printInfo() const
{
    std::cout << "Sphere: Radius = " << getRadius() << "\n"
              << "        Area = " << getArea() << "\n"
              << "        Volume = " << getVolume() <<"\n\n";
}

