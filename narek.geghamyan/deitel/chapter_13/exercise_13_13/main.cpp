#include <iostream>
#include <unistd.h>
#include "headers/Square.hpp"
#include "headers/Triangle.hpp"
#include "headers/Sphere.hpp"
#include "headers/Cube.hpp"


int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input square side: ";
    }
    double squareSide;
    std::cin >> squareSide;
    if (squareSide < 0) {
        std::cerr << "Error 1: Invalid square side !\a" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input triangle side: ";
    }
    double triangleSide;
    std::cin >> triangleSide;
    if (triangleSide < 0) {
        std::cerr << "Error 2: Invalid triangel side !\a" << std::endl;
        return 2;
    }


    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input sphere radius: ";
    }
    double radius;
    std::cin >> radius;
    if (radius < 0) {
        std::cerr << "Error 3: Invalid radius !\a" << std::endl;
        return 3;
    }


    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input cube side: ";
    }
    double cubeSide;
    std::cin >> cubeSide;
    if (cubeSide < 0) {
        std::cerr << "Error 4: Invalid cube side !\a" << std::endl;
        return 4;
    }

    const int SHAPES_NUMBER = 4;
    Shape* shapes[SHAPES_NUMBER] = {new Square(squareSide), new Triangle(triangleSide), new Sphere(radius), new Cube(cubeSide)};

    for (int i = 0; i < SHAPES_NUMBER; ++i) {
        shapes[i]->printInfo();
    }
}

