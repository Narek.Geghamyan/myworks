Distinguish between the terms “fatal error” and “non-fatal
mistake". Why might a fatal error be more desirable?

1) if the error is non-fatal, then it leads to an incorrect result, and if the error is fatal, then the program stops working further




2) a non-fatal error can lead to an incorrect result, and a fatal error will not give us any results and we will know that we wrote the code incorrectly
