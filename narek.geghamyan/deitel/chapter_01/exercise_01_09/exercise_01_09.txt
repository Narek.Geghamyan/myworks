Give a brief answer to each of the following questions:

a. Why does this text discuss structured programming in addition to object-oriented programming?

(structural programming is part of the oop)




b. What are the typical steps (mentioned in the text) of an object-oriented design process?

(To implement the best solutions, you must follow a detailed process of analyzing the requirements of your assignment  and developing a project that satisfies them.)



c. What kinds of messages do people send to one another?


(people send each other a message using speech, emotions, body movements)



d. Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user (a person object)?

(the machine's radio is controlled by a button and a screen)
