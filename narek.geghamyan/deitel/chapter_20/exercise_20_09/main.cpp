#include <iostream>
#include <vector>
#include <unistd.h>


template <typename T>
void
sortVector(std::vector<T>& v)
{
    for (size_t i = 0; i < v.size(); ++i) {
        for (size_t j = 0; j < v.size() - 1; ++j) {
            if (v[j + 1] < v[j]) {
                std::swap(v[j + 1], v[j]);
            }
        }
    }
}

template <typename T>
bool
check(const std::vector<T>& v)
{
    for (size_t i = 0; i < v.size() - 1; ++i) {
        if (v[i] > v[i + 1]) {
            return false;
        }
    }
    return true;
}

template <typename T>
void
printVector(const std::vector<T>& v)
{
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
int
binaryRecursiveSearch(const std::vector<T>& v, const T& key, const int left, const int right)
{
    const int mid = (left + right) / 2;
    if (key == v[mid]) {
        return mid;
    }

    if (left == right) {
        return -1;
    }

    if (key > v[mid]) {
        return binaryRecursiveSearch(v, key, mid + 1, v.size() - 1);
    }
    
    if (key < v[mid]) {
        return binaryRecursiveSearch(v, key, 0, mid - 1);
    }

    return -1;
}

template <typename T>
void
setVector(std::vector<T>& v)
{
    int number;
    for (size_t i = 0; i < 10; ++i) {
        std::cin >> number;
        v.push_back(number);
    }
}

int
main()
{
    std::vector<int> v;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 elements for vor: ";
    }

    setVector(v);
    std::cout << "Vector: ";
    printVector(v);
    
    if (!check(v)) {
        sortVector(v);
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input key: ";
    }
    int key;
    std::cin >> key;
    std::cout << "The "<< key << " is at index " << binaryRecursiveSearch(v, key, 0, v.size() - 1) << std::endl;
    return 0;
}

