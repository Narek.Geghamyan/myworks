#include <iostream>
#include <vector>
#include <unistd.h>

template <typename T>
void
setVector(std::vector<T>& v)
{
    int number;
    for (int i = 0; i < 10; ++i) {
        std::cin >> number;
        v.push_back(number);
    }
}

template <typename T>
void
printVector(const std::vector<T>& v)
{
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
int
partition (std::vector<T>& v, const int low, const int high)
{
    const int pivot = v[high];
    int i = low - 1;
 
    for (int j = low; j < high; ++j) {
        if (v[j] <= pivot) {
            ++i;
            std::swap(v[i], v[j]);
        }
    }
    std::swap(v[i + 1], v[high]);
    return (i + 1);
}

template <typename T>
void
quickSort(std::vector<T>& v, const int low, const int high)
{
    if (low < high) {
        const int pi = partition(v, low, high);
        quickSort(v, low, pi - 1);
        quickSort(v, pi + 1, high);
    }
}

int
main()
{
    std::vector<int> v;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 elements for vector: ";
    }
    setVector(v);
    std::cout << "Original vector: ";
    printVector(v);
    quickSort(v, 0, static_cast<int>(v.size() - 1));
    std::cout << "Sorted vector: ";
    printVector(v);
    return 0;
}

