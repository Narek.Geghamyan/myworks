#include <iostream>
#include <vector>
#include <unistd.h>
#include <cstdlib>

template <typename T>
int
search(const std::vector<T>& vect, const T& key, const int index = 0)
{
    if (index == static_cast<int>(vect.size())) {
        return -1;
    }

    if (key == vect[index]) {
        return index;
    }

    return search(vect, key, index + 1);
}

template <typename T>
void
setElements(std::vector<T>& vect)
{
    for (size_t i = 0; i < 10; ++i) {
        vect.push_back(1 + ::rand() % 50);
    }
}

template <typename T>
void
printVector(const std::vector<T>& vect)
{
    for (size_t i = 0; i != vect.size(); ++i) {
        std::cout << vect[i] << " ";
    }
    std::cout << std::endl;
}

int
main()
{
    std::vector<int> vect;
    setElements(vect);
    std::cout << "Vector: ";
    printVector(vect);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input key: ";
    }
    int key;
    std::cin >> key;

    if (-1 == search(vect, key)) {
        std::cout << "Key was not found.\a" << std::endl;
        return 0;
    }

    std::cout << "The key in the vector is at index " << search(vect, key) << std::endl;
    return 0;
}

