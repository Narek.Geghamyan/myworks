#include <iostream>
#include <unistd.h>
#include <vector>

void
bubbleSort(std::vector<int>& vect) /// The complexity of this algorithm is O(n^2), because in the worst case we will take n^2 steps to sort.
{
    for (size_t i = 0; i < vect.size(); ++i) {
        for (size_t j = 0; j < vect.size() - 1; ++j) { /// Improving this algorithm on the next problem.
            if (vect[j] > vect[j + 1]) {
                std::swap(vect[j], vect[j + 1]);
            }
        }
    }
}

void
print(const std::vector<int>& vect)
{
    for (size_t i = 0; i < vect.size(); ++i) {
        std::cout << vect[i] << " ";
    }
    std::cout << std::endl;
}

void
setVect(std::vector<int>& vect)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 elements of vector: ";
    }
    int number;
    for (int i = 0; i < 10; ++i) {
        std::cin >> number;
        vect.push_back(number);
    }
}

int
main()
{
    std::vector<int> vect;
    setVect(vect);
    print(vect);
    bubbleSort(vect);
    print(vect);
    return 0;
}

