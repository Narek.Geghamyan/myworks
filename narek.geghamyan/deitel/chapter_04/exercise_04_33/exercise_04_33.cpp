#include <iostream>

int
main()
{
    std::cout << "Input three numbers: ";
    int side1;
    std::cin >> side1;
    if (side1 <= 0) {
        std::cout << "Error 1: The side of a triangle cannot be negative or zero" << std::endl;
        return 1;
    }

    int side2;
    std::cin >> side2;
    if (side1 <= 0) {
        std::cout << "Error 1: The side of a triangle cannot be negative or zero" << std::endl;
        return 1;
    }

    int side3;
    std::cin >> side3;
    if (side1 <= 0) {
        std::cout << "Error 1: The side of a triangle cannot be negative or zero" << std::endl;
        return 1;
    }

    int side1Square = side1 * side1;
    int side2Square = side2 * side2;
    int side3Square = side3 * side3;

    if  (side1Square + side2Square == side3Square) {
        std::cout << "These numbers can make up the sides of a right triangle" << std::endl;
        return 0;
    }
    
    if  (side2Square + side3Square == side1Square) {
        std::cout << "These numbers can make up the sides of a right triangle" << std::endl;
        return 0;
    
    }
    
    if (side1Square + side3Square == side2Square) {
        std::cout << "These numbers can make up the sides of a right triangle" << std::endl;
        return 0;
    }

    std::cout << "These numbers cannot make up the sides of a right triangle" << std::endl;
    return 0;
}
