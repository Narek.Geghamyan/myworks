#include <iostream>

int
main()
{
    std::cout << "Input a five-digit number: ";
    int number;
    std::cin >> number;

    if (number < 10000) {
        std::cout << "Error 1: This number is not five-digit" << std::endl;
        return 1;
    }

    if (number > 99999) {
        std::cout << "Error 1: This number is not five-digit" << std::endl;
        return 1;
    }

    if (number / 10000 == number % 10) {
        if (number / 1000 % 10 == number / 10 % 10){
            std::cout << "This number is palindrome" << std::endl;
            return 0;
        }
    }

    std::cout << "This number is not palindrome" << std::endl;
    return 0;
}
