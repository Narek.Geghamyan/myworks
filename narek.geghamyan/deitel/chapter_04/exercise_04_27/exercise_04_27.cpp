#include <iostream>

int
main()
{
    std::cout << "Enter a binary number: ";
    int number;
    std::cin >> number;

    int decimalNumber = 0;
    int bit = 1;

    while (number != 0) {
        int lastDigit = number % 10;
        if (lastDigit != 0) {
            if (lastDigit != 1) {
                std::cout << "Error 1: This number is cannot be binary" << std::endl;
                return 1;
            }
        }

        decimalNumber += lastDigit  * bit;
        bit *= 2;
        number /= 10;
    }

    std::cout << "Decimal number: " << decimalNumber << std::endl;
    return 0;
}
