#include <iostream>

int
main()
{
    int square;
    std::cout << "Enter a number from one to twenty: ";
    std::cin >> square;

    if (square <= 0) {
        std::cout << "Error 1: The numbers of the stars cannot be negative or cannot be zero." << std::endl;
        return 1;
    }

    if (square > 20) {
        std::cout << "Error 2: The number should not be more than twenty." << std::endl;
        return 2;
    } 

    int row = 1;

    while (row <= square) {
        int counter = 1;

        while (counter <= square) {
            if (1 == row) {
                std::cout << "*";

            } else if (row == square) {
                std::cout << "*";

            } else if (1 == counter ) {
                std::cout << "*";
            
            } else if (counter == square) {
                std::cout << "*";
            
            } else {
                std::cout << " ";
            }
             ++counter;
        }
        ++row;
        std::cout << std::endl;
    }

    return 0;
} 

