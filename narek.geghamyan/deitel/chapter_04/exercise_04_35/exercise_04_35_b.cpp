#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Input the accuracy: ";
    std::cin >> accuracy;
    
    int number = 1;
    int factorial = 1;
    double e = 1.0;

    while (number < accuracy) {
        factorial *= number;
        e += 1.0 / factorial;
        ++number;
    }

    std::cout << "e = " << e << std::endl;
    return 0;
}

