#include <iostream>

int
main()
{
    int number;
    std::cout << "Input number: ";
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: The number cannot be negative." << std::endl;
        return 1;
    }
    
    int factorial = 1;
    int counter = 1;
    while (counter <= number) {
         factorial *= counter;
         ++counter;    
    }
    
    std::cout << number << "! = " << factorial << std::endl;
    return 0;
}

