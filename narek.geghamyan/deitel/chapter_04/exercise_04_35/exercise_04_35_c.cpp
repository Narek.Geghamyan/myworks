#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Input desired accuracy of e: ";
    std::cin >> accuracy;
 
    int x;
    std::cout << "Input exponent: ";
    std::cin >> x;

    int number = 1;
    int factorial = 1;
    int e = 1.0;
    int exponent = 1.0;
    while (number < accuracy) {
        exponent *= x;
        factorial *= number;
        e += exponent / factorial;
        ++number;
    }
    std::cout << "e ^ " << x << " is " << e << std::endl;
    return 0;
}
