#include <iostream>
#include <climits>

int
main()
{
    int counter = 0;
    int largest = INT_MIN; 
    int secondLargest = largest;
    std::cout << "Input ten numbers: ";
    while (counter < 10) {
        int number;
        std::cin >> number;
        if (number > largest) {
            secondLargest = largest;
            largest = number;
        } else if (number > secondLargest) {
            secondLargest = number; 
        }       
        ++counter;
    }
    std::cout << "The largest: " << largest << std::endl;
    std::cout << "The second largest: " << secondLargest << std::endl;

    return 0;
}

