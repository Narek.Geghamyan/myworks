#include <iostream>

/// The program will print powers of two as long as those digits fit into 4 bytes, Then it will start printing zeros.
/// When a digit is multiplied by 2, it shuffles to the left by one bit and when it hits 2 ^ 31, all bits will be zero.
/// When the bit reaches 2 ^ 31, the last bit is changed to 1, which means that the sign bit is changed, and a digit is obtained -2147483648

int
main()
{       
    int two = 2;
    while (true) {
        std::cout << two * 2 << std::endl;
        two += two;
    }

    return 0;
}

