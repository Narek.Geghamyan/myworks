#include <iostream>

int
main()
{
    std::cout << "Input three numbers: ";
    double side1;
    std::cin >> side1;
    if (side1 < 0) {
        std::cout << "Error 1: Sides cannot be negative." << std::endl;
        return 1;
    } 
    double side2;
    std::cin >> side2;
    if (side2 < 0) {
        std::cout << "Error 1: Sides cannot be negative." << std::endl;
        return 1;
    }
    double side3;
    std::cin >> side3;
    if (side3 < 0) {
        std::cout << "Error 1: Sides cannot be negative." << std::endl;
        return 1;    
    }
    if (side1 + side2 > side3) {
        if (side2 + side3 > side1) {
            if (side3 + side1 > side2) {
                std::cout << "These numbers can represent the sides of a triangle." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "These numbers cannot represent the sides of a triangle." << std::endl;
    return 0;
}

