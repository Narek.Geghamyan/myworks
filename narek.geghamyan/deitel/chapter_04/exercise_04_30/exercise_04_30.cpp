#include <iostream>

int
main()
{
    double pi = 3.14159;
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;

    if (radius <= 0) {
        std::cout << "Error 1: Radius cannot be negative or zero." << std::endl;
        return 1;
    }

    std::cout << "The diameter is: " << 2 * radius << std::endl;
    std::cout << "The circumferance is: " << pi * (2 * radius) << std::endl;
    std::cout << "The area of a circleis: " << pi * (radius * radius) << std::endl;

    return 0;
}
     
