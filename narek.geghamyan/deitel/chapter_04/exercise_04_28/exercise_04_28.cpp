#include <iostream>

int
main()
{
    int count = 1;

    while (count <= 8) {
        if (count % 2 == 0) {
            std::cout << ' ';
        }
        int space = 1;
        while (space <= 8) {
            std::cout << "* ";
            ++space;
        }     
        std::cout << std::endl;
        ++count;
    }
    return 0;
} 
