#include <iostream>

int
main()
{
    int counter = 0;
    int largest = -2147483648;
    std::cout << "Input ten numbers: ";

    while (counter < 10) {
        int number;
        std::cin >> number;
        ++counter;
        if (number > largest) {
            largest = number;
        }
    }

    std::cout << "The largest: " << largest << std::endl;
    return 0;
}

