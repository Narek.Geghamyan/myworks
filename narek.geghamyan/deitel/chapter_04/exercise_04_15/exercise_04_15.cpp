#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {
        std::cout << "Enter sales in dollars (-1 to end): ";
        float sales;
        std::cin >> sales;
        if (std::abs(-1 - sales) < 0.001) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }
        float salary = 200 + (sales * 0.09);
        std::cout << "Salary: " << std::setprecision(2) << std::fixed << salary << std::endl;
    }
    return 0;
}
     
