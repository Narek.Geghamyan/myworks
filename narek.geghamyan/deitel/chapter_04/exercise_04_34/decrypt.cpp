#include <iostream>

int
main()
{
    std::cout << "Input number: ";
    int number;
    std::cin >> number;
    
    if (number < 0) {
        std::cerr << "Error 1: This number is negative." << std::endl;
        return 1;
    }

    if (number < 1000) {
        std::cerr << "Error 2: This number is not four-digit." << std::endl;
        return 2;
    }

    if (number > 9999) {
        std::cerr << "Error 2: This number is not four-digit." << std::endl;
        return 2;
    }

    int digit1 = (number / 1000 + 3) % 10;
    int digit2 = (number % 1000 / 100 + 3) % 10;
    int digit3 = (number % 100 / 10 + 3) % 10;
    int digit4 = (number % 10 + 3) % 10;

    std::cout << "Decrypted number: " << digit1 << digit2 << digit3 << digit4 << std::endl;

    return 0;
}

