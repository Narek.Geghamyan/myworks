#include <iostream>
#include "Analysis.hpp"

int
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCounter = 1;
  
    while (studentCounter <= 10) {
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        int result;
        std::cin >> result;
  
        if (1 == result) {
            ++passes;
        } else if (2 == result) {
            ++failures;
        } else {
            std::cout << "Error 1: You cannot enter a number other than 1 or 2" << std::endl;
            return 1;
        }

        ++studentCounter;
    }

    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;

    if (passes > 8) {
        std::cout << "Raise tuition" << std::endl;
    }

    return 0;
}
  
