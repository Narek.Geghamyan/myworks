#include <iostream>
#include "Analysis.hpp"

int
main()
{
    Analysis application;
    return application.processExamResults();
}

