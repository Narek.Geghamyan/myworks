#include <iostream>
#include <iomanip>

int
main()
{
    while (true) {        
        std::cout << "Enter hours worked (-1 to end): ";
        float hours;
        std::cin >> hours;

        if (static_cast<int>(hours) == -1) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (hours < 0) {
            std::cout << "Error 1: The number of hours cannot be negative." << std::endl;
            return 1;
        }

        std::cout << "Enter hourly rate of the worker ($00.00): ";
        float rate;
        std::cin >> rate;

        if (rate < 0) {
            std::cout << "Error 2: The number of rate cannot be negative." << std::endl;
            return 2;
        }
        
        float salary = hours * rate;
        if (hours > 40) {
            float extraHours = hours - 40;
            salary += extraHours * rate / 2;
        }

        std::cout << "Salary: " << std::setprecision(2) << std::fixed << salary << "\n" << std::endl;
    }

    return 0;
}

