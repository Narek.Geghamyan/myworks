#include <iostream>
#include "Account.hpp"

Account::Account(int accountNumber, float accountBalance, float outBalance, float inBalance, float creditLimit)
{
    setAccountNumber(accountNumber);
    setAccountBalance(accountBalance);
    setOutBalance(outBalance);
    setInBalance(inBalance);
    setCreditLimit(creditLimit);
}

void
Account::setAccountNumber(int accountNumber)
{
    accountNumber_ = accountNumber;
}

void
Account::setAccountBalance(float accountBalance)
{
    accountBalance_ = accountBalance;
}

void 
Account::setOutBalance(float outBalance)
{
    outBalance_ = outBalance;
}

void
Account::setInBalance(float inBalance)
{
    inBalance_ = inBalance;
}

void 
Account::setCreditLimit(float creditLimit)
{
    creditLimit_ = creditLimit;
}

int
Account::getAccountNumber()
{
    return accountNumber_;
}

float
Account::getAccountBalance()
{
    return accountBalance_;
}

float
Account::getOutBalance()
{
    return outBalance_;
}

float
Account::getInBalance()
{
    return inBalance_;
}

float
Account::getCreditLimit()
{
    return creditLimit_;
}
