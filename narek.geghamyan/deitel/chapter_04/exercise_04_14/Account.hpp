class Account
{
public:
    Account(int accountNumber, float accountBalance, float outAccountBalance, float inAccountBalance, float creditLimit);
    void setAccountNumber(int accountNumber);
    void setAccountBalance(float accountBalance); 
    void setOutBalance(float outAccountBalance);
    void setInBalance(float inaccountBalance);
    void setCreditLimit(float creditLimit);
    int getAccountNumber();
    float getAccountBalance();
    float getOutBalance();
    float getInBalance();
    float getCreditLimit();
private:
    int accountNumber_;
    float accountBalance_;
    float outBalance_;
    float inBalance_;
    float creditLimit_;
}; 
