#include <iostream>
#include <iomanip>
#include "Account.hpp"

int
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Enter account number (-1 to end): ";
        std::cin >> accountNumber;
    
        if (accountNumber == -1) {
            return 0;
        }

        float accountBalance;
        std::cout << "Enter beginning balance: ";
        std::cin >> accountBalance;
        float outBalance;
        std::cout << "Enter total charges: ";
        std::cin >> outBalance;
        float inBalance;
        std::cout << "Enter total credits: ";
        std::cin >> inBalance;
        float creditLimit;
        std::cout << "Enter credit limit: ";
        std::cin >> creditLimit;

        Account user(accountNumber, accountBalance + outBalance - inBalance, outBalance, inBalance, creditLimit);      
        std::cout << "New balance is " << user.getAccountBalance() << std::endl;

        if (user.getAccountBalance() > user.getCreditLimit()) {
            std::cout << "Account: " << user.getAccountNumber() << std::endl;
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << user.getCreditLimit() << std::endl;
            std::cout << "Balance: " << user.getAccountBalance() << std::endl;
            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    }
    
    return 0;
}

