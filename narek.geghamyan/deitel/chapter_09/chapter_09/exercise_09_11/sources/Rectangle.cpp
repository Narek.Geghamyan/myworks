#include "headers/Rectangle.hpp"
#include <cassert>

Rectangle::Rectangle(const double lenght, const double width)
{
    setLenght(lenght);
    setWidth(width);
}

void
Rectangle::setLenght(const double lenght)
{
    assert(lenght > 0.0 && lenght < 20.0);
    lenght_ = lenght;
}

void
Rectangle::setWidth(const double width)
{
    assert(width > 0.0 && width < 20.0);
    width_ = width; 
}

double
Rectangle::perimeter() const
{
    const double perimeter = (lenght_ + width_) * 2;
    return perimeter;
}

double
Rectangle::area() const
{
    const int area = lenght_ * width_;
    return area;
}

