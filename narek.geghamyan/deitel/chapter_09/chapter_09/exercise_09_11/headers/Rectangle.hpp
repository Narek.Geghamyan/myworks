#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const double lenght = 1, const double width = 1);
    double perimeter() const;
    double area() const;
    void setLenght(const double lenght);
    void setWidth(const double width);
private:
    double lenght_;
    double width_;
};
#endif /// __RECTANGLE_HPP_

