#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <cassert>
#include <iostream>

int
Player::play(Board* board, const int row, const int column) const
{
    assert(board != NULL);
    return board->setMark(row, column, playerMark_);
}

int
Player::getMark() const
{
    return playerMark_;
}
