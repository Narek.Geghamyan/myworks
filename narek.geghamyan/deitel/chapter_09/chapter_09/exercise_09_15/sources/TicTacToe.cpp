#include "headers/TicTacToe.hpp"
#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <iostream>
#include <cassert>
#include <unistd.h>

int
TicTacToe::play(Board* board, Player* player1, Player* player2) const
{
    switch (playLoop(board, player1, player2)) {
    case -1: std::cout << "Draw !"         << std::endl; break;
    case 0: std::cout << "Player 1 Win !" << std::endl; break;
    case 1: std::cout << "Player 2 Win !" << std::endl; break;
    default: assert(false);
    }
    return 0;
}

bool
TicTacToe::isWin(Board* board, Player* player, const int playerNumber) const
{
    int row = 0;
    int column = 0;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Player " << playerNumber + 1 << " input coordinates (row, column): ";
        }
        std::cin >> row >> column;
        switch (player->play(board, row, column)) {
        case 0: break;
        case 1: std::cout << "Warning 1: You went out of the playing field\a" << std::endl; continue;
        case 2: std::cout << "Warning 2: This cell is busy.\a" << std::endl; continue;
        default: assert(false);
        }
        return board->isWin(row, column, 'x') || board->isWin(row, column, 'o');
    }
}

int
TicTacToe::playLoop(Board* board, Player* player1, Player* player2) const 
{
    const size_t PLAYER_COUNT = 2;
    Player* players[PLAYER_COUNT] = { player1, player2 };
    while (true) {
        for (size_t i = 0; i < PLAYER_COUNT; ++i) {
            if (isWin(board, players[i], i)) {
                return i;
            }
        }
    }
    return -1;
}

void
TicTacToe::help() const
{
    std::cout << "\tInstruction." << std::endl;
    std::cout << "1. The game is played on a grid that's 3 squares by 3 squares.\n"
              << "2. You are X, your friend is O. Players take turns putting their marks in empty squares.\n"
              << "3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.\n" << std::endl;
}

