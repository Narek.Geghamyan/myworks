#include "headers/TicTacToe.hpp"
#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <iostream>
#include <cstring>

int
main(int argc, char* argv[])
{
    TicTacToe ticTacToe;
    if (2 == argc && 0 == ::strcmp(argv[1], "-help")) {
        ticTacToe.help();
    }

    Board board;
    board.printBoard(std::cout);
    Player player1('x');
    Player player2('o');
    return ticTacToe.play(&board, &player1, &player2);
}

