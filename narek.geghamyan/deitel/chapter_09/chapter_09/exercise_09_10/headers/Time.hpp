#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time() : hour_(0), minute_(0), second_(0) {}
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    int setTime(const int hour, const int minute, const int second);
    int setHour(const int hour);
    int setSecond(const int second);
    int setMinute(const int minute);
    void tickTime();
    void tickHour();
    void tickSecond();
    void tickMinute();
    void print() const;
private:
    int hour_;
    int minute_;
    int second_;
};
#endif /// __TIME_HPP__

