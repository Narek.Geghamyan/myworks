#include "headers/DateAndTime.hpp"
#include <iostream>
#include <cassert>
#include <iomanip>

static const int monthDays[13] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

DateAndTime::DateAndTime(const int year, const int month, const int day, const int hour, const int minute, const int second)
{
    setDate(year, month, day);
    setTime(hour, minute, second);
}

void
DateAndTime::nextYear()
{
    ++year_;
}


void
DateAndTime::nextMonth()
{
    ++month_;
    if (month_ > 12) {
        month_ = 1;
        nextYear();
    }
}

void
DateAndTime::nextDay()
{
    ++day_;

    if ((day_ > monthDays[month_]) || (!isLeapYear() && day_ == monthDays[2])) {
        day_ = 1;
        nextMonth();
    }
}
bool
DateAndTime::isLeapYear() const
{
    return ((0 == year_ % 400) || (year_ % 100 != 0 && year_ % 4 == 0));
}

void
DateAndTime::setDate(const int year, const int month, const int day)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

void
DateAndTime::setYear(const int year)
{
    assert(year > 0);
    year_ = year;
}

void
DateAndTime::setMonth(const int month)
{
    assert(month > 0 && month <= 12);
    month_ = month;
}

void
DateAndTime::setDay(const int day)
{
    assert(day > 0 && day < 31);
    day_ = day;
}

void
DateAndTime::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
DateAndTime::setHour(const int hour)
{
    assert(hour < 24 && hour >= 0);
    hour_ = hour;
}

void
DateAndTime::setMinute(const int minute)
{
    assert(minute < 60 && minute >= 0);
    minute_ = minute;
}

void
DateAndTime::setSecond(const int second)
{
    assert(second < 60 && second >= 0);
    second_ = second;
}

void
DateAndTime::nextTime()
{
    nextSecond();
}

void
DateAndTime::nextHour()
{
    ++hour_;
    if (24 == hour_) {
        hour_ = 0;
        nextDay();
    }
}

void
DateAndTime::nextMinute()
{
    ++minute_;
    if (60 == minute_) {
        minute_ = 0;
        nextHour();
    }
}

void
DateAndTime::nextSecond()
{
    ++second_;
    if (60 == second_) {
        second_ = 0;
        nextMinute();
    }
}

int
DateAndTime::getHour() const
{
    return hour_;
}

int
DateAndTime::getMinute() const
{
    return minute_;
}

int
DateAndTime::getSecond() const
{
    return second_;
}

int
DateAndTime::getYear() const
{
    return year_;
}

int
DateAndTime::getMonth() const
{
    return month_;
}

int
DateAndTime::getDay() const
{
    return day_;
}

void
DateAndTime::printTime() const
{
    printDate();
    std::cout << "\t" << std::setfill('0') << std::setw(2) << getHour()   << ":"
                                   << std::setw(2) << getMinute() << ":"
                                   << std::setw(2) << getSecond()
                                   << std::setfill(' ') << std::endl;
}

void
DateAndTime::printDate() const
{
    std::cout << getDay() << '/' << getMonth() << '/' << getYear();
}

