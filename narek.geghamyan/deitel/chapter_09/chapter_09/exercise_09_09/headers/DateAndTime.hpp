#ifndef __DATE_AND_TIME_HPP__
#define __DATE_AND_TIME_HPP__

class DateAndTime
{
public:
    DateAndTime(const int year = 1, const int month = 1, const int day = 1, const int hour = 0, const int minute = 0, const int second = 0);
    int getYear() const;
    int getMonth() const;
    int getDay() const;
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    void setDate(const int year, const int month, const int day);
    void setYear(const int year);
    void setMonth(const int month);
    void setDay(const int dey);
    void setTime(const int hour, const int minute, const int second);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    void nextYear();
    void nextMonth();
    void nextDay();
    void nextHour();
    void nextMinute();
    void nextSecond();
    void nextTime();
    void printDate() const;
    void printTime() const;
    bool isLeapYear() const;
private:
    int year_;
    int month_;
    int day_;
    int hour_;
    int minute_;
    int second_;
};
#endif /// __DATE_AND_TIME_HPP__

