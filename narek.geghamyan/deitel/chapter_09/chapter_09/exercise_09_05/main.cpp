#include "headers/Complex.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Complex number 1.\n";
        std::cout << "Input two numbers: ";
    }
    int number1, number2;
    std::cin >> number1 >> number2;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Complex number 2.\n";
        std::cout << "Input two numbers: ";
    }
    int number3, number4;
    std::cin >> number3 >> number4;

    Complex complexNumber1(number1, number2);
    Complex complexNumber2(number3, number4);

    std::cout << "Complex number 1: ";
    complexNumber1.print();
    std::cout << std::endl;

    std::cout << "Complex number 2: ";
    complexNumber2.print();
    std::cout << std::endl;

    const Complex totalPlus = complexNumber1.plus(complexNumber2);
    complexNumber1.print();
    std::cout << " + ";
    complexNumber2.print();
    std::cout << " = ";
    totalPlus.print();
    std::cout << std::endl;

    const Complex totalMinus = complexNumber1.minus(complexNumber2);
    complexNumber1.print();
    std::cout  << " + " ;
    complexNumber2.print();
    std::cout << " = ";
    totalMinus.print();
    std::cout << std::endl;
    return 0;
}

