#include "headers/Rational.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Rational number1." << std::endl;
        std::cout << "Input numerator: ";
    }
    int numerator1;
    std::cin >> numerator1;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input denominator: ";
    }
    int denominator1;
    std::cin >> denominator1;
    Rational rationalNumber1(numerator1, denominator1);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Rational number2." << std::endl;
        std::cout << "Input numerator: ";
    }
    int numerator2;
    std::cin >> numerator2;
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input denominator: ";
    }
    int denominator2;
    std::cin >> denominator2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Do you need an abbreviated answer or not (0-Yes, 1-No): ";
    }

    int select;
    std::cin >> select;
    if (select != 0 && select != 1) {
       std::cerr << "Error 1: There is no such option to choose from.\a" << std::endl;
    }


    Rational rationalNumber2(numerator2, denominator2);

    Rational plusTotal = rationalNumber1.plus(rationalNumber2);
    rationalNumber1.printRational();
    std::cout << " + ";
    rationalNumber2.printRational();
    std::cout << " = " << std::setprecision(2);
    if (0 == select) {
        plusTotal.normalize();
    }
    plusTotal.printRational();
    std::cout << std::endl;

    Rational minusTotal = rationalNumber1.minus(rationalNumber2);
    rationalNumber1.printRational();
    std::cout << " - ";
    rationalNumber2.printRational();
    std::cout << " = " << std::setprecision(2);
    if (0 == select) {
        minusTotal.normalize();
    }
    minusTotal.printRational();
    std::cout << std::endl;

    Rational multiplicationTotal = rationalNumber1.multiplication(rationalNumber2);
    rationalNumber1.printRational();
    std::cout << " * ";
    rationalNumber2.printRational();
    std::cout << " = " << std::setprecision(2);
    if (0 == select) {
        multiplicationTotal.normalize();
    }
    multiplicationTotal.printRational();
    std::cout << std::endl;
    
    Rational divisionTotal = rationalNumber1.division(rationalNumber2);
    rationalNumber1.printRational();
    std::cout << " / ";
    rationalNumber2.printRational();
    std::cout << " = " << std::setprecision(2);
    if (0 == select) {
        divisionTotal.normalize();
    }
    divisionTotal.printRational();
    std::cout << std::endl;

    std::cout << "\nRational number 1: ";
    rationalNumber1.printRational();
    std::cout << "\nRational number 2: ";
    rationalNumber2.printRational();

    std::cout << "\nRational number 1 (float): ";
    rationalNumber1.printFloatRational();
 
    std::cout << "Rational number 2 (float): ";
    rationalNumber2.printFloatRational();
    return 0;
}

