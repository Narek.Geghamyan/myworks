#include "headers/Date.hpp"
#include <iostream>
#include <cassert>

static const int monthDays[13] = {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

Date::Date(const int day, const int month, const int year)
{
    assert(day >= 0 && day <= 31 && month >= 0 && month <= 12 && year > 0);
    day_ = day;
    month_ = month;
    year_ = year;
}

void
Date::print() const
{
    std::cout << day_ << '/' << month_ << '/' << year_ << std::endl;
}

void
Date::nextDay()
{
    ++day_;

    if ((day_ > monthDays[month_]) || (!isLeapYear() && day_ == monthDays[2])) {
        day_ = 1;
        nextMonth();
    }
}

void
Date::nextMonth()
{
    ++month_;
    if (month_ > 12) {
        month_ = 1;
        nextYear();
    }
}

void
Date::nextYear()
{
    ++year_;
}

bool
Date::isLeapYear() const
{
    return ((0 == year_ % 400) || (year_ % 100 != 0 && year_ % 4 == 0));
}

