#ifndef __BOARD_HPP__
#define __BOARD_HPP__

#include <iostream>

class Board
{ 
    static const int BOARD_SIZE = 3;
public:
    Board();
    void printBoard(std::ostream& out) const;
    int setMark(const int row, const int column, const char playerMark);
    bool isWin(const int row, const int column, const char playerMark) const;
private:
    void constructBoard();
    bool isRowWin(const int row, const char playerMark) const;
    bool isColumnWin(const int column, const char playerMark) const;
    bool isDiagonalWin(const char playerMark) const;
    bool isLeftDiagonalWin(const char playerMark) const;
    bool isRightDiagonalWin(const char playerMark) const;
private:
    char board_[BOARD_SIZE][BOARD_SIZE];
};

#endif /// __BOARD_HPP__

