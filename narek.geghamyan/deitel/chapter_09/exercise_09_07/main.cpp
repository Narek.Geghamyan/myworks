#include "headers/Time.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    Time time1(23, 59, 50);
    std::cout << "Time: ";
    time1.print();
    std::cout << "Incrementation.\n";

    for (int i = 0; i < 15; ++i) {
        time1.tickTime();
        time1.print();
    }
    return 0;
}

