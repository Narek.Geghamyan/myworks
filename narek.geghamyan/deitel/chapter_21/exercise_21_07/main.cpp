#include <iostream>
#include <stdlib.h>
#include "headers/SingleList.hpp"

typedef typename cd01::SingleList<int>::const_iterator const_iterator;

void
setElements(cd01::SingleList<int>& slist)
{
    for (size_t i = 0; i < 10; ++i) {
        slist.push_back(1 + ::rand() % 50);
    }
}

void
printList(const cd01::SingleList<int>& slist)
{
    for (const_iterator it = slist.begin(); it != slist.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;
}

void
marge(const cd01::SingleList<int>& slist1, const cd01::SingleList<int>& slist2, cd01::SingleList<int>& slist3)
{
    for (const_iterator it = slist1.begin(); it != slist1.end(); ++it) {
        slist3.push_back(*it);
    }
  
    for (const_iterator it = slist2.begin(); it != slist2.end(); ++it) {
        slist3.push_back(*it);
    }
}

int
main()
{
    cd01::SingleList<int> slist1;
    setElements(slist1);
    
    cd01::SingleList<int> slist2;
    setElements(slist2);
    
    cd01::SingleList<int> slist3;
    
    std::cout << "SingleLIst 1: ";
    printList(slist1);
    std::cout << "SingleLIst 2: ";
    printList(slist2);
    marge(slist1, slist2, slist3);
    std::cout << "Marge...\nNew SingleList: ";
    printList(slist3);
    return 0;
}

