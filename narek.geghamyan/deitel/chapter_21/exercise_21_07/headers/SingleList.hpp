#ifndef __T_SINGLELIST_HPP__
#define __T_SINGLELIST_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class SingleList;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::SingleList<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::SingleList<T>& rhv);

namespace cd01 {

template <typename T>
class SingleList
{
private:
    struct Node {
        Node();
        Node(const T data, Node* next = NULL);
        T data_;
        Node* next_;
    };

    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const SingleList<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, SingleList<Type>& rhv);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef Node* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
 
   
    class const_iterator {
        friend SingleList<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        const_iterator& operator++();
        const_iterator operator++(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    protected:
        explicit const_iterator(pointer ptr);
        void setPtr(const pointer ptr); 
        pointer node() const;
        pointer next();
        void setNext(pointer node);
    private:
        pointer ptr_;
    };
 
    class iterator : public const_iterator {
        friend SingleList<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        value_type* operator->() const;
        iterator& operator=(const iterator& rhv);
        value_type& operator*() const;
        iterator& operator++();
        iterator operator++(int);
    private:
        explicit iterator(const pointer ptr);
    };

public:
    SingleList();
    SingleList(const size_type size);
    SingleList(const int size, const T& initialValue);
    SingleList(const SingleList<T>& rhv);
    template <typename InputIterator>
    SingleList(InputIterator first, InputIterator last);
    ~SingleList();
    size_type size() const;
    bool empty() const;
    void resize(const size_type n);
    void resize(const size_type n, const T& initialValue);
    void push_back(const T& elem);
    void push_front(const T& elem);
    void pop_back();
    void pop_front();
    const SingleList& operator=(const SingleList<T>& rhv);
    bool operator==(const SingleList<T>& rhv) const;
    bool operator!=(const SingleList<T>& rhv) const;
    bool operator<(const SingleList<T>& rhv) const;
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    void concatenate(const SingleList<T>& rhv);
    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const T& arg);
    void insert_after(iterator pos, const int n, const T& arg);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator l, InputIterator r);
 
    iterator insert_before(iterator pos);
    iterator insert_before(iterator pos, const T& arg);
    iterator insert_before(iterator pos, const int n, const T& arg);
    template <typename InputIterator>
    void insert_before(iterator pos, InputIterator l, InputIterator r);

    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);
private:
    Node* begin_;
    Node* end_;

}; /// end of template class SingleList

} /// end of namespace cd01

#include "sources/SingleList.cpp"

#endif /// __T_SINGLELIST_HPP__

