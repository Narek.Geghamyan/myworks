#include "headers/SingleList.hpp"

typedef typename cd01::SingleList<int>::const_iterator const_iterator;

void
setElements(cd01::SingleList<int>& l)
{
    for (size_t i = 0; i < 10; ++i) {
        l.push_back(1 + ::rand() % 50);
    }
}

void
printList(const cd01::SingleList<int>& l)
{
    for (const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;
}

int
main()
{
    cd01::SingleList<int> l;
    setElements(l);
    std::cout << "SingleLIst 1: ";
    printList(l);

    cd01::SingleList<int> s;
    setElements(s);
    std::cout << "SingleList 2: ";
    printList(s);

    l.concatenate(s);

    std::cout << "Concatenate...\nNew SingleList 1: ";
    printList(l);
    return 0;
}
