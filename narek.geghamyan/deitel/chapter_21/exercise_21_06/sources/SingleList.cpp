#include "headers/SingleList.hpp"

#include <cassert>
#include <cstdlib>
#include <iostream>

/// SingleList<T>

template <typename T>
std::ostream&
operator<<(std::ostream& out, const cd01::SingleList<T>& rhv)
{
    typedef typename cd01::SingleList<T>::const_iterator const_iterator;
    out << "( ";
    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        out << *it << ' ';
    }
    out << ')';
    return out;
}

template <typename T>
std::istream&
operator>>(std::istream& in, cd01::SingleList<T>& rhv)
{
    typedef typename cd01::SingleList<T>::const_iterator const_iterator;
    const char obr = static_cast<char>(in.get());
    if (obr != '(' && obr != '{' && obr != '[') {
        in.setstate(std::ios_base::failbit);
        return in;
    }

    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        in >> *it;
    }

    const char cbr = static_cast<char>(in.get());
    if (cbr != ')' && cbr != '}' && cbr != ']') {
        in.setstate(std::ios_base::failbit);
        return in;
    }
    return in;
}

namespace cd01 {

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL)
    , end_(NULL)
{ 
}

template <typename T>
SingleList<T>::SingleList(const size_type size)
    : begin_(NULL)
    , end_(NULL)
{
    resize(size);
}

template <typename T>
SingleList<T>::SingleList(const int size, const T& initialValue) 
    : begin_(NULL)
    , end_(NULL)
{
    resize(size, initialValue);
}

template <typename T>
SingleList<T>::SingleList(const SingleList<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    resize(rhv.size());
    for (const_iterator it1 = begin(), *it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
        *it1 = *it2;
    }
}

template <typename T>
template <typename InputIterator>
SingleList<T>::SingleList(InputIterator first, InputIterator last)
    : begin_(NULL)
    , end_(NULL)
{
    insert_after(begin(), first, last);
}

template <typename T>
SingleList<T>::~SingleList()
{
    resize(0);
    if (begin_ != NULL) {
        begin_ = end_ = NULL;
    }
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return const_iterator();
}


template <typename T>
const SingleList<T>&
SingleList<T>::operator=(const SingleList<T>& rhv)
{
    typedef typename cd01::SingleList<T>::const_iterator const_iterator;
    typedef typename cd01::SingleList<T>::iterator iterator;
    if (&rhv == this) {
        return *this;
    }


    if (size() != rhv.size()) { 
        resize(rhv.size());
        iterator it1 = begin();
        for (const_iterator it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
            *it1 = *it2;
        }
    }
    return *this;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    size_type counter = 0;
    for (const_iterator it = begin(); it != end(); ++it, ++counter);
    return counter;
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return size() == 0;
}

template <typename T>
void
SingleList<T>::concatenate(const SingleList<T>& rhv)
{
    for (const_iterator it = rhv.begin(); it != rhv.end(); ++it) {
        push_back(*it);
    }
}

template <typename T>
bool
SingleList<T>::operator==(const SingleList<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    for (const_iterator it1 = begin(), it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
        if (*it1 != *it2) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
SingleList<T>::operator!=(const SingleList<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
SingleList<T>::operator<(const SingleList<T>& rhv) const
{
    typedef cd01::SingleList<T>::const_iterator const_iterator;

    if (size() < rhv.size()) {
        return true;
    }

    for (const_iterator it1 = begin(), it2 = rhv.begin(); it1 != end(); ++it1, ++it2) {
        if (*it1 > *it2) {
            return false;
        }
    }
    return true;
}

template <typename T>
void
SingleList<T>::resize(const size_type n)
{
    size_type listSize = size(); 
    if (n == listSize) {
        return;
    }
    
    if (n > listSize) {
        for (size_t i = 0; i < n - listSize; ++i) {
            push_back(T());
        } 
        return;
    }

    iterator pos = begin();
    for (size_t i = 0; i < n; ++i, ++pos);
    erase(pos, end());
}

template <typename T>
void
SingleList<T>::resize(const size_type n, const T& initialValue)
{
    size_type listSize = size(); 
    if (n == listSize) {
        return;
    }
    
    if (n > listSize) {
        for (size_t i = 0; i < n - listSize; ++i) {
            push_back(initialValue);
        } 
        return;
    }

    iterator pos = begin();
    for (size_t i = 0; i < n; ++i, ++pos);
    erase(pos, end());
}

template <typename T>
void
SingleList<T>::push_back(const T& elem)
{
    end_ = new Node(elem, end_);
    if (end_->next_ != NULL) {
        end_->next_->next_ = end_;
        end_->next_ = NULL;
    }
    if (begin().node() == NULL) {
        begin_ = end_;
    }
}

template <typename T>
void
SingleList<T>::push_front(const T& elem)
{
    begin_ = new Node(elem, begin_);
}

template <typename T>
void
SingleList<T>::pop_back()
{
    assert(begin_ != NULL);

    if (NULL == begin_->next_) {
       erase(begin());
       return;
    }
    
    pointer tmp = begin_;
    for (; tmp->next_->next_ != NULL; tmp = tmp->next_);

    delete tmp->next_;
    tmp->next_ = NULL; 
    end_ = tmp;
}

template <typename T>
void
SingleList<T>::pop_front()
{
    assert(begin_ != NULL);
    erase(begin());
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(SingleList<T>::iterator pos, const T& arg)
{
    if (begin() == pos && NULL == begin_) {
        push_front(arg);
        return begin();
    }
    
    if (pos == end()) {
        push_back(arg);
        return pos;
    }

    pos.setNext(new Node(arg, pos.next()));
    pos.ptr_ = pos.next();
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(SingleList<T>::iterator pos)
{
    if (begin() == pos && NULL == begin_) {
        push_front(T());
        return begin();
    }
    
    if (pos == end()) {
        push_back(T());
        return pos;
    }

    pos.setNext(new Node(T(), pos.next()));
    pos.ptr_ = pos.next();
    return pos;

}

template <typename T>
void
SingleList<T>::insert_after(SingleList<T>::iterator pos, const int n, const T& arg)
{
    for (int i = 0; i < n; ++i) {
        pos = insert_after(pos, arg);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert_after(SingleList<T>::iterator pos, InputIterator first, InputIterator last)
{
    for (; first != last; ++first) {
        pos = insert_after(pos, *first);
   }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_before(SingleList<T>::iterator pos, const T& arg)
{
    insert_after(pos, arg);
    std::swap(*pos, pos.next()->data_);
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_before(SingleList<T>::iterator pos)
{
    insert_after(pos, T());
    std::swap(*pos, pos.next()->data_);
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_before(SingleList<T>::iterator pos, const int n, const T& arg)
{
    for (int i = 0; i < n; ++i) {
        pos = insert_before(pos, arg);
    }
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos)
{
    assert(pos.ptr_ != NULL);
    
    if (NULL == pos.next() && pos != begin()) {
        iterator tmp = begin();
        for (iterator it = tmp; it != pos; tmp = it++);
        tmp.setNext(pos.next());
        delete pos.ptr_;
        return tmp;
    }

    if (pos == begin()) {
        begin_ = begin_->next_;
        delete pos.ptr_;
        return begin();
    }

    std::swap(pos.ptr_->data_, pos.ptr_->next_->data_);
    pointer n = pos.ptr_->next_->next_;
    delete pos.ptr_->next_;
    pos.ptr_->next_ = n;
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator first, iterator last)
{
    if (last == first) {
        return first;
    }

    if (first == begin()) {
        begin_ = last.ptr_;
    } else {
        iterator tmp = begin();
        for (iterator it = tmp; it != first; tmp = it++);
        tmp.setNext(last.ptr_);
    }

    do {
        iterator tmp = first++;
        delete tmp.ptr_;
    } while (first != last);

    return last;
}

/// SingleList<T>::const_iterator

template <typename T>
SingleList<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
SingleList<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(SingleList<T>::pointer ptr)
    : ptr_(ptr)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(const SingleList<T>::const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
void
SingleList<T>::const_iterator::setNext(pointer node)
{
    ptr_->next_ = node;
}

template <typename T>
const typename SingleList<T>::value_type&
SingleList<T>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator=(const const_iterator& rhv)
{
    if (ptr_ == rhv.ptr_) {
        return *this;
    }
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const typename SingleList<T>::value_type* 
SingleList<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator++()
{ 
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const const_iterator tmp(ptr_);
    ptr_ = ptr_->next_;
    return tmp;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename T>
typename SingleList<T>::pointer
SingleList<T>::const_iterator::node() const
{
    return ptr_;
}

template <typename T>
void
SingleList<T>::const_iterator::setPtr(const pointer ptr)
{
    ptr_ = ptr;
}

template <typename T>
typename SingleList<T>::pointer
SingleList<T>::const_iterator::next() 
{
    return ptr_->next_;
}

/// SingleList<T>::iterator

template <typename T>
SingleList<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
SingleList<T>::iterator::iterator(const pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
SingleList<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
SingleList<T>::iterator::~iterator()
{
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator();
}

template <typename T>
typename SingleList<T>::value_type*
SingleList<T>::iterator::operator->() const
{
    return const_iterator::node();
}

template <typename T>
typename SingleList<T>::iterator&
SingleList<T>::iterator::operator=(const iterator& rhv)
{ 
    const_iterator::operator=(rhv);
    return *this;
}

template <typename T>
typename SingleList<T>::value_type&
SingleList<T>::iterator::operator*() const
{
    return const_iterator::node()->data_;
}

template <typename T>
typename SingleList<T>::iterator&
SingleList<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::node()->next_);
    return *this;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::iterator::operator++(int)
{
    const iterator tmp(const_iterator::node());
    const_iterator::operator++();
    return tmp;
}

template <typename T>
SingleList<T>::Node::Node() 
    : data_(T())
    , next_(NULL)
{
}

template <typename T>
SingleList<T>::Node::Node(const T data, Node* next)
    : data_(data)
    , next_(next) 
{
}

} /// end of namespace cd01


