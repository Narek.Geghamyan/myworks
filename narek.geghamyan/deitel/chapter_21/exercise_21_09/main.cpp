#include <iostream>
#include <stdlib.h>
#include "headers/SingleList.hpp"

typedef typename cd01::SingleList<char>::const_iterator const_iterator;

void
printList(const cd01::SingleList<char>& l)
{
    for (const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;
}

void
setElements(cd01::SingleList<char>& l)
{
    for (size_t i = 0; i < 10; ++i) {
        l.push_back('a' + i);
    }
}

void
reverse(const cd01::SingleList<char>& l, cd01::SingleList<char>& rl)
{
    for (const_iterator it = l.begin(); it != l.end(); ++it) {
        rl.push_front(*it);
    }
}

int
main()
{
    cd01::SingleList<char> l;
    setElements(l);
    std::cout << "SingleList 1: ";
    printList(l);
    cd01::SingleList<char> rl;
    reverse(l, rl);
    std::cout << "SingleList 2: ";
    printList(rl);
    return 0;
}

