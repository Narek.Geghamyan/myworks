#include <iostream> 

int
main()
{
    for (int counter1 = 0; counter1 < 5; ++counter1) {
        std::cout << "Input Number (1-30): ";
        int number;
        std::cin >> number;
        if (number < 1 || number > 30) {
            std::cerr << "Error 1: Out of range digit entered." << std::endl;
            return 1;
        }

        for (int counter2 = 0; counter2 < number; ++counter2) {
            std::cout << "* ";
        }

        std::cout << std::endl;
    }

    return 0;
}
