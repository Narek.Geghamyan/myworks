#include <iostream>
#include <limits>

int
main()
{
    std::cout << "Input quantity of numbers: ";
    int quantity;
    std::cin >> quantity;
    int smallest = std::numeric_limits<int>::max();

    for (int counter = 0; counter < quantity; ++counter) {
        std::cout << "Input number: ";
        int number;
        std::cin >> number;
        if (number <= smallest) {
            smallest = number;
        }
    }

    std::cout << "Smallest: " << smallest << std::endl;

    return 0;
}

