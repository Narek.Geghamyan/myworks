#include <iostream>

int
main()
{    
    int factorial = 1;               /// calculating the factorial of 20 might be difficult, because it might be such a large number that it would not fit in an int

    for (int counter = 1; counter <= 20; ++counter) {
        factorial *= counter;
        std::cout << counter << "! = " << factorial << std::endl;
    }

    return 0;
}
         
