#include <iostream>
#include <iomanip>

int
main()
{ 
    for (int counter1 = 0; counter1 < 10; ++counter1) {
        for (int counter2 = 0; counter2 < 10; ++counter2) {
            if (counter2 <= counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }  
        }

        std::cout << std::setw(4);
        
        for (int counter3 = 10; counter3 > 0; --counter3 ) {
            if ( counter3 > counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        
        std::cout << std::setw(4);

        for (int counter4 = 0; counter4 < 10; ++counter4) {
            if (counter4 >= counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::setw(4);

        for (int counter5 = 10; counter5 >= 0; --counter5) {
            if (counter5 <= counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
    
        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int counter1 = 0; counter1 < 10; ++counter1) {
        for (int counter2 = 0; counter2 < 10; ++counter2) {
            if (counter2 <= counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::endl;
    }
    
    std::cout << std::endl;

    for (int counter1 = 10; counter1 > 0; --counter1) {
        for (int counter2 = 0; counter2 < 10; ++counter2) {
            if (counter1 <= counter2) {
                std::cout << " ";
            } else {
                std::cout << "*";
            }
        }

        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int counter1 = 0; counter1 < 10; ++counter1) {
        for (int counter2 = 0; counter2 < 10; ++counter2) {
            if (counter2 < counter1) {
                std::cout << " ";
            } else {
                std::cout << "*";
            }
        }

        std::cout << std::endl;
    }
    
    std::cout << std::endl;

    for (int counter1 = 10; counter1 > 0; --counter1) {
         for (int counter2 = 0; counter2 < 10; ++counter2) {
             if (counter2 >= counter1) {
                 std::cout << "*";
             } else {
                 std::cout << " ";
             }
         }

         std::cout << std::endl;
    }
            
    return 0;
}

