#include <iostream>
#include <cstring>
#include "GradeBook.hpp"

int
main()
{
    std::cout << "Input course name: ";
    std::string courseName;
    std::getline(std::cin, courseName);
    std::cout << "Input teacher name: ";
    std::string teacherName;
    std::getline(std::cin, teacherName);

    GradeBook examResult (courseName, teacherName);
    examResult.printCourseName();
    return examResult.inputGrades();
}

