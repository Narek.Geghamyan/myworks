#include <iostream>
#include <cstring>
#include "GradeBook.hpp"


GradeBook::GradeBook(std::string courseName, std::string teacherName)
{
    GradeBook::setCourseName(courseName);
    GradeBook::setTeacherName(teacherName);
}

void
GradeBook::setCourseName(std::string courseName)
{
    courseName_ = courseName;
}

void
GradeBook::setTeacherName(std::string teacherName)
{
    teacherName_ = teacherName;
}

int
GradeBook::inputGrades()
{              
    int aCount = 0;
    int bCount = 0;
    int cCount = 0;
    int dCount = 0;
    double sum = 0;
    int grade;
    
    std::cout << "Input grade (A, B, C, D):\n";
    while ((grade = std::cin.get()) != EOF) {
        switch (grade) {
        case 'A': case 'a': sum += 4; ++aCount; break;
        case 'B': case 'b': sum += 3; ++bCount; break;
        case 'C': case 'c': sum += 2; ++cCount; break;
        case 'D': case 'd': sum += 1; ++dCount; break;
        case '\n': case '\t': case ' ': break;
        default: std::cout << "Error 1: This grade is not correct.\n"; return 1;
        }
    }

    std::cout << std::endl;
    std::cout << "A = " << aCount << std::endl;
    std::cout << "B = " << bCount << std::endl;
    std::cout << "C = " << cCount << std::endl;
    std::cout << "D = " << dCount << std::endl;
    int counterSum = aCount + bCount + cCount + dCount;
    if (0 == counterSum) {
        std::cout << "Error 2: The number of grades cannot be zero" << std::endl;
        return 2;
    }

    std::cout << "Average: " << sum / counterSum << std::endl;    
    return 0;
}

void
GradeBook::printCourseName()
{
    std::cout << "Welcome to the " <<GradeBook::getCourseName() << std::endl;
    std::cout << "Teacher of this cours " << GradeBook::getTeacherName() << std::endl;
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

std::string
GradeBook::getTeacherName()
{
    return teacherName_;
}
