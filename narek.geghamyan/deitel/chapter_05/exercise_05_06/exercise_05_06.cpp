#include <iostream>

int
main()
{
    int sum = 0;
    int counter = 0;

    while (true) {
        std::cout << "Input number (9999 to exit): ";
        int number;
        std::cin >> number;
        if (9999 == number) {
            break;
        } 
        
        sum += number;
        ++counter;
    }
     
    std::cout << "Average number: " << sum / counter << std::endl; 

    return 0;
}

