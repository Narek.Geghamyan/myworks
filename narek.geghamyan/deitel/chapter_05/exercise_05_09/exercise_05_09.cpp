#include <iostream>

int
main()
{
    int productOfNumbers = 1;

    for (int counter = 1; counter <= 15; counter += 2) {
            productOfNumbers *= counter;
    }

    std::cout << "Product of odd numbers: " << productOfNumbers << std::endl;

    return 0;
}

