#include <iostream>

int
main()
{
    /// a)
    std::cout << "Input two numbers: ";
    int x, y;
    std::cin >> x >> y;
    if ((!(x < 5) && !(y >= 7)) == (!(x < 5 || y >= 7))) {
        std::cout << "For x = " << x << ", y = " << y << std::endl;
        std::cout << "!(x < 5) && !(y >= 7) is equivalent to !(x < 5 || y >= 7)" << std::endl;
    } else {
        std::cout << "!(x < 5) && !(y >= 7) is not equivalent to !(x < 5 || y >= 7)" << std::endl;
    }
    
    std::cout << "\n";

    /// b)
    std::cout << "Input three numbers: ";
    int a, b, g;
    std::cin >> a >> b >> g;
    if ((!(a == b) || !(g != 5)) == (!(a == b && g != 5))) {
        std::cout << "For a = " << a << ", b = " << b << ", g = " << g << std::endl;
        std::cout << "!(а == Ь) || !(g != 5) is equivalent to !(a == b && g != 5)" << std::endl;
    } else {
        std::cout << "!(а == Ь) || !(g != 5) is not equivalent to !(a == b && g != 5)" << std::endl;
    }

    std::cout << "\n";
    
    /// c)
    std::cout << "Input two numbers: ";
    int q, z;
    std::cin >> q >> z;
    if ((!(q <= 8 && z > 4 ) == (q > 8 || z <= 4))) {
        std::cout << "For q = " << q << ", z = " << z << std::endl;
        std::cout << "!(q <= 8 && z > 4) is equivalent to (q > 8 || z <= 4)" << std::endl;
    } else {
        std::cout << "!(q <= 8 && z > 4) is not equivalent to (q > 8 || z <= 4)" << std::endl;
    }

    std::cout << "\n";
    
    /// d)
    std::cout << "Input two numbers: ";
    int i, j;
    std::cin >> i >> j;
    if (!(i > 4 || j <= 6) == (i <= 4 && j > 6)) {
        std::cout << "For i = " << i << ", j = " << j << std::endl;
        std::cout << "!(i > 4 || j <= 6) is equivalent to (i <= 4 && j > 6)" << std::endl;
    } else {
        std::cout << "!(i > 4 || j <= 6) is not equivalent to (i <= 4 && j > 6)" << std::endl;
    }

    return 0;
}

