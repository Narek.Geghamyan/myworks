#include <iostream>

int
main()
{
    int growth = 9;
    int center = growth / 2;

    for (int counter1 = -center; counter1 <= center; ++counter1) {
        for (int counter2 = -center; counter2 <= center; ++counter2) {
            if (std::abs(counter1) + std::abs(counter2) > center) {
                std::cout << " ";
            } else {
                std::cout << "*";
            }
        }

        std::cout << std::endl;
    }
    
    return 0;
}
       
