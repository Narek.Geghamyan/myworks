#include <iostream>
#include <iomanip>

int
main()
{
    int rate = 5;
    int amount = 100000;
    std::cout << "Year\t" << "Amount" << std::endl;
    std::cout << std::setprecision(2) << std::endl;

    for (int year = 1; year <= 10; ++year) {
        amount +=  amount * rate / 100;
        int cent = amount % 100;
        std::cout << year << "\t" << amount / 100 << ".";

        if (cent < 10) {
            std::cout << "0";
        }

        std::cout << cent << std::endl;        
    }

    return 0;
}
