#include <iostream>
#include <iomanip>

int
main()
{
    int precision = 100;
    double pi = 0.0;
    int plus = 1;
    int minus = 3;
    double piGolden = 3.14159;
    int counter = 1;

    while (precision < 1000000) {
        pi += 4.0 / plus - 4.0 / minus;

        double nonDotPi = piGolden * precision;

        if (static_cast<int>(nonDotPi) - static_cast<int>(pi * precision) == 0) {
            std::cout << counter << " " << pi << std::endl;
            precision *= 10;
        }
        
        minus += 4;
        plus += 4;
        ++counter;
    }
    return 0;
}

