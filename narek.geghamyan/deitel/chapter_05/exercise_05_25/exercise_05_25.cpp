#include <iostream>

int
main()
{
    for (int counter = 1; counter <= 10; ++counter) {
        std::cout << counter << " ";

        if (4 == counter) {
            counter = 10;
        }
    }

    std::cout << std::endl;

    return 0;
}
