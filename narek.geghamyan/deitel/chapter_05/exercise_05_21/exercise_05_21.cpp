#include <iostream>

int
main()
{
    std::cout << "Input worker code: ";
    int workerCode;
    std::cin >> workerCode;
    
    switch (workerCode) {
    case 1:
        int manager;
        std::cout << "Input manager salary: ";
        std::cin >> manager;
        std::cout << "Weekly salary: " << manager << std::endl;
        break;
    case 2:
        std::cout << "Worked: ";
        int hour;
        std::cin >> hour;
        std::cout << "Input salary: ";
        int hourPrice;
        std::cin >> hourPrice;
        std::cout << "Salary: " << hour * hourPrice << "\n";
        break;
    case 3:
        std::cout << "Weekly sales (in dollars): ";
        int sold; std::cin >> sold;
        double commWorker; commWorker = sold * (0.057) + 250;
        std::cout << "Salary: " << commWorker << "\n";
        break;
    case 4:
        std::cout << "Sold per week: ";
        int product;
        std::cin >> product;
        std::cout << "Input price: ";
        int price;
        std::cin >> price;
        std::cout << "Salary: " << price * product << "\n";
        break;
    default:
        std::cerr << "Error 1: Working code is not correct." << std::endl;
        return 1;
    }

    return 0;
}

