#include <iostream>

int
main()
{
    std::cout << "Enter an odd number (1 - 19): ";
    int growth;
    std::cin >> growth;
    if (growth % 2 == 0 || growth > 19 || growth < 1) {
        std::cerr << "Error 1: Incorrect number entered." << std::endl;
        return 1;
    }

    int center = growth / 2;
    for (int counter1 = 0; counter1 <= center; ++counter1) {
        for (int counter2 = 0; counter2 < growth * 2; ++counter2) {
            if (counter2 <= center + counter1 && counter2 >= center - counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::endl;
    }

     for (int counter1 = 1; counter1 <= center; ++counter1) {
        for (int counter2 = 0; counter2 < growth * 2; ++counter2) {
            if (counter2 >= counter1 && counter2 < growth  - counter1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::endl;
    }

    return 0;
}

