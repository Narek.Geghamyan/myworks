#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::fixed << std::setprecision(2);
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "Year" << std::setw(99) << "Amount on deposit(" << rate << "%)\n"<< std::endl;
        double precent = rate / 100.0;
        double amount = 24.00;
        for (int year = 1626; year <= 2021; ++year) {
            amount += amount * precent;
            std::cout << year << std::setw(100) << amount << "$" << std::endl;
        }

        std::cout << std::endl;
    }

    return 0;
}

