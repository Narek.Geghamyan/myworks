#include <iostream>

int
main()
{
    std::cout << "Number\t" << "Binary\t\t" << "Octa\t" << "Hexa" << std::endl;
    int binary = 512;
    for (int number = 1; number <= 256; ++number) {
        std::cout << number << "\t";
        for (int counter = 2; counter <= 16; counter *= 2) {
            if (4 == counter) {
                continue;
            }

            for (binary = 4096; binary >= 1; binary /= counter) {
                int number2 = (number / binary) % counter;
                switch (number2) {
                case 10: std::cout << "A"; break;
                case 11: std::cout << "B"; break;
                case 12: std::cout << "C"; break;
                case 13: std::cout << "D"; break;
                case 14: std::cout << "E"; break;
                case 15: std::cout << "F"; break;
                default: std::cout << number2; break;
                }
            }
            
            if (16 == counter) {
                std::cout << std::endl;
            } else {
                std::cout << "\t";
            }

        }
    }


    return 0;
}

