#include <iostream>

int
main()
{
    std::cout << "Input amount of numbers: ";
    int amountOfNumbers;
    std::cin >> amountOfNumbers;

    int sum = 0;

    for (int counter = 0; counter < amountOfNumbers; ++counter) {
        std::cout << "Input number: ";
        int number;
        std::cin >> number;
        sum += number;
    }

    std::cout << "Numbers sum: " << sum << std::endl;

    return 0;
}

