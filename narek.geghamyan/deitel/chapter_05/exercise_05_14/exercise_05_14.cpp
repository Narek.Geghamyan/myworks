#include <iostream>
#include <iomanip>

int
main()
{
    float total = 0;

    while (true) {
        std::cout << "Enter product number (1 - 5) (-1 to stop): ";
        int productNumber;
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        }
        
        if (productNumber <= 0 || productNumber > 5) {
            std::cout << "Error 1: Product with this number does not exist." << std::endl;
            return 1;
        }

        std::cout << "Enter the number of items sold per day: ";
        int perDay;
        std::cin >> perDay;

        if (perDay < 0) {
            std::cout << "Error 2: The number of sales cannot be negative" << std::endl;
            return 2;
        }

        switch (productNumber) {
        case 1: total += perDay * 2.98; break;
        case 2: total += perDay * 4.50; break;
        case 3: total += perDay * 9.98; break;
        case 4: total += perDay * 4.49; break;
        case 5: total += perDay * 6.87; break;
        }
    }

    std::cout << "Total: " << std::fixed << std::setprecision(2) << total << "$" << std::endl; 

    return 0;
}

