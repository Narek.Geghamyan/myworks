#include <iostream>
#include <cmath>

int
main()
{
    /// if side1 == side2
    /// 2side1 ^ 2 = hypotenuse ^ 2
    /// sqrt(2) * side1 = hypotenuse

    int limit = 500;
    std::cout << "Side 1\tSide 2\tHypotenuse" << std::endl;
    for (int side1 = 3; side1 <= static_cast<int>(limit / std::sqrt(2)); ++side1) {
        int side1Square = side1 * side1;
        for (int side2 = side1 + 1; side2 <= limit; ++side2) {
            int hypotenuse = std::sqrt(side1Square + side2 * side2);
            if (hypotenuse > limit) {
                break;
            }

            if (hypotenuse * hypotenuse == side1Square + side2 * side2) {
                std::cout << side1 << "\t" << side2 << "\t" << hypotenuse << std::endl;
            }
        }
    }

    return 0;
}

