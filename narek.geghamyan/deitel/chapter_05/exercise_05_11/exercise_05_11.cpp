#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout <<  std::fixed << std::setprecision(2);
        std::cout << "Year" << std::setw(21) << "Amount on deposit " << rate << "%" << std::endl;
       
        double amount = 1000; 
        double percent = rate / 100.00;

        for (int year = 1; year <= 10; ++year) {
            amount += amount * percent;
            std::cout << std::setw(4) << year << std::setw(21) << std::fixed << std::setprecision(2) << amount << std::endl;
        }

        std::cout << std::endl;
    }

    return 0;
}
