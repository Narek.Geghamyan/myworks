#include "headers/Player.hpp"
#include "headers/Battle.hpp"

int
main()
{
    Board board;
    Player player1;
    Player player2;
    Battle game;
    game.Play(player1, player2, board);
    return 0;
}

