#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include "headers/Battle.hpp"

void
Battle::Play(Player& player1, Player& player2, Board board)
{
    for (int i = 0; i < 30; ++i) {
        board.printBoard();
        player1.attack(board);
        board.printBoard();
        player2.attack(board);
    }
}

