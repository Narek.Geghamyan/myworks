#include "headers/Board.hpp"
#include "headers/Player.hpp"
#include <iostream>
#include <iomanip>

Board::Board()
{
    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 10; ++j) {
            board_[i][j] = ' ';
        }
    }
}

void
Board::printBoard()
{
    std::cout << "    A B C D E F G H I J" << std::endl;
    for (int i = 0; i < 10; ++i) {
        std::cout << std::setw(2) << i + 1 << "  ";
        for (int j = 0; j < 10; ++j) {
            std::cout << board_[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void
Board::setMark(char coordinate1, int coordinate2, Board& board)
{
    switch (coordinate1) {
    case 'A':
    case 'a':
    coordinate1 = 0; break;
    case 'B':
    case 'b':
    coordinate1 = 1; break;
    case 'C':
    case 'c':
    coordinate1 = 2; break;
    case 'D':
    case 'd':
    coordinate1 = 3; break;
    case 'E':
    case 'e':
    coordinate1 = 4; break;
    case 'F':
    case 'f':
    coordinate1 = 5; break;
    case 'G':
    case 'g':
    coordinate1 = 6; break;
    case 'H':
    case 'h':
    coordinate1 = 7; break;
    case 'I':
    case 'i':
    coordinate1 = 8; break;
    case 'J':
    case 'j':
    coordinate1 = 9; break;
    default:
    std::cout << "Invalide simbol !\a" << std::endl;
    break;
    }

    if (coordinate2 < 0 || coordinate2 > 10) {
        std::cout << "Worning!: Invalide coordinates ! " << std::endl;
        return;
    }
    board.board_[coordinate2 - 1][static_cast<int>(coordinate1)]  = '.';
}

