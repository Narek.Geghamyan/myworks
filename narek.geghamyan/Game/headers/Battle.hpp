#ifndef __BATTLE_HPP__
#define __BATTLE_HPP__

#include "headers/Board.hpp"
#include "headers/Player.hpp"

class Battle
{
public:
    void Play(Player& player1, Player& player2, Board board);
};

#endif /// __BATTLE_HPP__

