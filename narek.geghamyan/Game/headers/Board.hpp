#ifndef __BOARD_HPP__
#define __BOARD_HPP__

class Board
{
public:
    Board();
    void printBoard(); 
    void setMark(char coordinate1, int coordinate2, Board& board);
private:
    char board_[10][10];
};

#endif /// __BOARD_HPP__

