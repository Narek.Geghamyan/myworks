#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

#include "headers/Board.hpp"

class Player
{
public:
    void attack(Board& board);
};

#endif /// __PLAYER_HPP__

